<%-- 
    Document   : test
    Created on : Jun 3, 2022, 5:07:14 PM
    Author     : kingo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="shortcut icon" href="img/fav.png" />
        <title>Happy Programming</title>
        <link rel="stylesheet" href="https://unpkg.com/@webpixels/css/dist/index.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js">
    </head>
    <body>
         Banner 
        <a href="https://webpixels.io/components?ref=codepen" class="btn w-full btn-primary text-truncate rounded-0 py-2 border-0 position-relative" style="z-index: 1000;">
            <strong>Crafted with Webpixels CSS:</strong> The design system for Bootstrap 5. Browse all components &rarr;
        </a>


        <div class="p-10 bg-surface-secondary" style="background-image: url(img/home-banner-bg.png);">
            <div class="container">
                <div class="card">
                    <div class="card-header">
                        <h6>Bootstrap 5 Table</h6>

                    </div>
                    <div class="table-responsive">
                        <table class="table table-hover table-nowrap">
                            <thead class="table-light">
                                <tr>
                                    <th scope="col">Name</th>
                                    <th scope="col">Job Title</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Phone</th>
                                    <th scope="col">Lead Score</th>
                                    <th scope="col">Company</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td data-label="Name">
                                        <img alt="..." src="https://images.unsplash.com/photo-1502823403499-6ccfcf4fb453?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=3&w=256&h=256&q=80" class="avatar avatar-sm rounded-circle me-2">
                                        <a class="text-heading font-semibold" href="#">
                                            Robert Fox
                                        </a>
                                    </td>
                                    <td data-label="Job Title">
                                        <span>Web Designer</span>
                                    </td>
                                    <td data-label="Email">
                                        <a class="text-current" href="mailto:robert.fox@example.com">robert.fox@example.com</a>
                                    </td>
                                    <td data-label="Phone">
                                        <a class="text-current" href="tel:202-555-0152">202-555-0152</a>
                                    </td>
                                    <td data-label="Lead Score">
                                        <span class="badge bg-soft-success text-success">7/10</span>
                                    </td>
                                    <td data-label="Company">
                                        <a class="text-current" href="#">Dribbble</a>
                                    </td>
                                    <td data-label="" class="text-end">
                                        <div class="dropdown">
                                            <a class="text-muted" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="bi bi-three-dots-vertical"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-end">
                                                <a href="#!" class="dropdown-item">
                                                    Action
                                                </a>
                                                <a href="#!" class="dropdown-item">
                                                    Another action
                                                </a>
                                                <a href="#!" class="dropdown-item">
                                                    Something else here
                                                </a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td data-label="Job Title">
                                        <img alt="..." src="https://images.unsplash.com/photo-1610271340738-726e199f0258?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=3&w=256&h=256&q=80" class="avatar avatar-sm rounded-circle me-2">
                                        <a class="text-heading font-semibold" href="#">
                                            Darlene Robertson
                                        </a>
                                    </td>
                                    <td data-label="Email">
                                        <span>Developer</span>
                                    </td>
                                    <td data-label="Phone">
                                        <a class="text-current" href="mailto:darlene@example.com">darlene@example.com</a>
                                    </td>
                                    <td data-label="Lead Score">
                                        <a class="text-current" href="tel:224-567-2662">224-567-2662</a>
                                    </td>
                                    <td data-label="Company">
                                        <span class="badge bg-soft-warning text-warning">5/10</span>
                                    </td>
                                    <td data-label="">
                                        <a class="text-current" href="#">Netguru</a>
                                    </td>
                                    <td data-label="" class="text-end">
                                        <div class="dropdown">
                                            <a class="text-muted" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="bi bi-three-dots-vertical"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-end">
                                                <a href="#!" class="dropdown-item">
                                                    Action
                                                </a>
                                                <a href="#!" class="dropdown-item">
                                                    Another action
                                                </a>
                                                <a href="#!" class="dropdown-item">
                                                    Something else here
                                                </a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td data-label="Job Title">
                                        <img alt="..." src="https://images.unsplash.com/photo-1610878722345-79c5eaf6a48c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=3&w=256&h=256&q=80" class="avatar avatar-sm rounded-circle me-2">
                                        <a class="text-heading font-semibold" href="#">
                                            Theresa Webb
                                        </a>
                                    </td>
                                    <td data-label="Email">
                                        <span>Marketing Specialist</span>
                                    </td>
                                    <td data-label="Phone">
                                        <a class="text-current" href="mailto:theresa.webb@example.com">theresa.webb@example.com</a>
                                    </td>
                                    <td data-label="Lead Score">
                                        <a class="text-current" href="tel:401-505-6800">401-505-6800</a>
                                    </td>
                                    <td data-label="Company">
                                        <span class="badge bg-soft-danger text-danger">2/10</span>
                                    </td>
                                    <td data-label="">
                                        <a class="text-current" href="#">Figma</a>
                                    </td>
                                    <td data-label="" class="text-end">
                                        <div class="dropdown">
                                            <a class="text-muted" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="bi bi-three-dots-vertical"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-end">
                                                <a href="#!" class="dropdown-item">
                                                    Action
                                                </a>
                                                <a href="#!" class="dropdown-item">
                                                    Another action
                                                </a>
                                                <a href="#!" class="dropdown-item">
                                                    Something else here
                                                </a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td data-label="Job Title">
                                        <img alt="..." src="https://images.unsplash.com/photo-1612422656768-d5e4ec31fac0?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=3&w=256&h=256&q=80" class="avatar avatar-sm rounded-circle me-2">
                                        <a class="text-heading font-semibold" href="#">
                                            Kristin Watson
                                        </a>
                                    </td>
                                    <td data-label="Email">
                                        <span>Sales Manager</span>
                                    </td>
                                    <td data-label="Phone">
                                        <a class="text-current" href="mailto:cody.fisher@example.com">cody.fisher@example.com</a>
                                    </td>
                                    <td data-label="Lead Score">
                                        <a class="text-current" href="tel:307-560-8817">307-560-8817</a>
                                    </td>
                                    <td data-label="Company">
                                        <span class="badge bg-soft-success text-success">9/10</span>
                                    </td>
                                    <td data-label="">
                                        <a class="text-current" href="#">Mailchimp</a>
                                    </td>
                                    <td data-label="" class="text-end">
                                        <div class="dropdown">
                                            <a class="text-muted" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="bi bi-three-dots-vertical"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-end">
                                                <a href="#!" class="dropdown-item">
                                                    Action
                                                </a>
                                                <a href="#!" class="dropdown-item">
                                                    Another action
                                                </a>
                                                <a href="#!" class="dropdown-item">
                                                    Something else here
                                                </a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td data-label="Job Title">
                                        <img alt="..." src="https://images.unsplash.com/photo-1502823403499-6ccfcf4fb453?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=3&w=256&h=256&q=80" class="avatar avatar-sm rounded-circle me-2">
                                        <a class="text-heading font-semibold" href="#">
                                            Robert Fox
                                        </a>
                                    </td>
                                    <td data-label="Email">
                                        <span>Web Designer</span>
                                    </td>
                                    <td data-label="Phone">
                                        <a class="text-current" href="mailto:robert.fox@example.com">robert.fox@example.com</a>
                                    </td>
                                    <td data-label="Lead Score">
                                        <a class="text-current" href="tel:202-555-0152">202-555-0152</a>
                                    </td>
                                    <td data-label="Company">
                                        <span class="badge bg-soft-success text-success">7/10</span>
                                    </td>
                                    <td data-label="">
                                        <a class="text-current" href="#">Dribbble</a>
                                    </td>
                                    <td data-label="" class="text-end">
                                        <div class="dropdown">
                                            <a class="text-muted" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="bi bi-three-dots-vertical"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-end">
                                                <a href="#!" class="dropdown-item">
                                                    Action
                                                </a>
                                                <a href="#!" class="dropdown-item">
                                                    Another action
                                                </a>
                                                <a href="#!" class="dropdown-item">
                                                    Something else here
                                                </a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
