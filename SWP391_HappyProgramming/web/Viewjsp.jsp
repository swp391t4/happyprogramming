<%-- 
    Document   : Viewjsp
    Created on : May 25, 2022, 9:04:21 PM
    Author     : Admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha1/dist/css/bootstrap.min.css">
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <style>
        table, th, td {
            border: 1px solid black;
        }

        input{
            margin-bottom: 20px;
        }
    </style>

    <body>
        <c:forEach items="${listP}" var="o">
            <div class="container rounded bg-white mt-5 mb-5">
                <div class="row">
                    <div class="col-md-3 border-right">
                        <div class="d-flex flex-column align-items-center text-center p-3 py-5"><img class="rounded-circle mt-5" width="150px" src="${o.img}"><span class="font-weight-bold">${o.name}</span><span class="text-black-50">${o.email}</span><span> </span></div>
                    </div>
                    <div class="col-md-5 border-right">
                        <div class="p-3 py-5">
                            <div class="d-flex justify-content-between align-items-center mb-3">
                                <h4 class="text-right">Profile Settings</h4>
                            </div>
                            <div class="row mt-2">
                                <div class="col-md-6"><label class="labels">Username</label><input type="text" class="form-control" placeholder="username" value="${o.username}" readonly=""></div>
                                <div class="col-md-6"><label class="labels">Password</label><input type="text" class="form-control" value="${o.password}" placeholder="password" readonly=""></div>
                            </div>
                            <div class="row mt-3">

                                <div class="col-md-12"><label class="labels">Name</label><input type="text" class="form-control" placeholder="name" value="${o.name}" readonly=""></div>
                                    <c:if test="${o.gender == 1}">
                                    <div class="col-md-12"><label class="labels">Gender</label><input type="text" class="form-control" placeholder="gender" value="Male" readonly=""></div>
                                    </c:if>
                                    <c:if test="${o.gender == 0}">
                                    <div class="col-md-12"><label class="labels">Gender</label><input type="text" class="form-control" placeholder="gender" value="Female" readonly=""></div>
                                    </c:if>
                                <div class="col-md-12"><label class="labels">Phone Number</label><input type="text" class="form-control" placeholder="phon number" value="${o.phone}" readonly=""></div>
                                <div class="col-md-12"><label class="labels">Address</label><input type="text" class="form-control" placeholder="address" value="${o.address}" readonly=""></div>
                                <div class="col-md-12"><label class="labels">DOB</label><input type="text" class="form-control" placeholder="date of births" value="${o.DOB}" readonly=""></div>
                                <div class="col-md-12"><label class="labels">serviceDesc</label><input type="text" class="form-control" placeholder="serviceDesc" value="${o.serviceDesc}" readonly=""></div>
                                <div class="col-md-12"><label class="labels">archivemnetDesc</label><input type="text" class="form-control" placeholder="archivementDesc" value="${o.archivementDesc}" readonly=""></div>
                            </div>

                            <div class="mt-5 text-center"><button class="btn btn-primary profile-button" type="button">Edit</button></div>
                        </div>

                    </div>
                    <div class="col-md-4">
                        <div class="p-3 py-5">
                            <c:if test="${o.role == 1}">
                                <div class="col-md-12"><label class="labels">Role</label><input type="text" class="form-control" placeholder="role" value="Mentor" readonly=""></div> <br>
                                </c:if>
                            <div class="col-md-12"><label class="labels">Profession</label><input type="text" class="form-control" placeholder="job" value="" readonly=""></div> <br>
                            <div class="col-md-12"><label class="labels">Intro</label><input type="text" class="form-control" placeholder="additional details" value="" readonly=""></div>
                        </div>
                        <c:forEach begin="1" end="${endP}" var="x">
                            <a href="ViewCVofMentor?index=${x}">${x}</a>
                        </c:forEach>
                    </div>
                </div>

            </div>

        </c:forEach>




    </body>
    <style>
        *{
            padding:0;
            margin:0;
            box-sizing: border-box;
        }
        img {
            width:100px;
        }
        body {
            background: rgb(99, 39, 120)
        }

        .form-control:focus {
            box-shadow: none;
            border-color: #BA68C8
        }

        .profile-button {
            background: rgb(99, 39, 120);
            box-shadow: none;
            border: none
        }

        .profile-button:hover {
            background: #682773
        }

        .profile-button:focus {
            background: #682773;
            box-shadow: none
        }

        .profile-button:active {
            background: #682773;
            box-shadow: none
        }

        .back:hover {
            color: #682773;
            cursor: pointer
        }

        .labels {
            font-size: 11px
        }

        .add-experience:hover {
            background: #BA68C8;
            color: #fff;
            cursor: pointer;
            border: solid 1px #BA68C8
        }
    </style>

</html>
