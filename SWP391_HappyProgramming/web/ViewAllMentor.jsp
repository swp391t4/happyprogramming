<%-- 
    Document   : ViewAllMentor
    Created on : Jun 28, 2022, 2:54:14 PM
    Author     : Admin
--%>

<%@page import="Model.User"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">       
        <!-- Mobile Specific Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <!-- Favicon -->
        <link rel="shortcut icon" href="img/fav.png" />
        <!-- Author Meta -->
        <meta name="author" content="colorlib" />
        <!-- Meta Description -->
        <meta name="description" content="" />
        <!-- Meta Keyword -->
        <meta name="keywords" content="" />
        <!-- meta character set -->
        <meta charset="UTF-8" />
        <!-- Site Title -->

        <title>Eclipse Education</title>

        <title>Happy Programming</title>


        <link href="https://fonts.googleapis.com/css?family=Playfair+Display:900|Roboto:400,400i,500,700" rel="stylesheet" />
        <!--
            CSS
            =============================================
        -->
        <link rel="stylesheet" href="css/linearicons.css" />
        <link rel="stylesheet" href="css/font-awesome.min.css" />
        <link rel="stylesheet" href="css/bootstrap.css" />
        <link rel="stylesheet" href="css/magnific-popup.css" />
        <link rel="stylesheet" href="css/owl.carousel.css" />
        <link rel="stylesheet" href="css/nice-select.css">
        <link rel="stylesheet" href="css/hexagons.min.css" />
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/themify-icons/0.1.2/css/themify-icons.css" />
        <link rel="stylesheet" href="css/main.css" />
    </head>
    <body>
        <!-- ================ Start Header Area ================= -->
        <header class="default-header">
            <nav class="navbar navbar-expand-lg  navbar-light">
                <div class="container">



                    <a class="navbar-brand" href="home">
                        <img style="width: 200px; height: auto" src="img/logo-6.png" alt="" />
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="lnr lnr-menu"></span>
                    </button>

                    <div class="collapse navbar-collapse justify-content-end align-items-center" id="navbarSupportedContent">
                        <ul class="navbar-nav">
                            
                            <li> <a href="view?id=ViewAllSkill">View list of skills</a></li>
                            <!-- Dropdown -->
                            <c:if test="${sessionScope.user != null}">                          
                                <li class="dropdown">
                                    <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                                        <img style="height: 35px; width: 35px; margin-top: -5px" src="${sessionScope.user.img}" class="avatar avatar-sm rounded-circle me-2">
                                    </a>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item fa fa-user s_color" style="font-weight: bold" href="#">${sessionScope.user.name}</a>
                                        <a class="dropdown-item" style="height: 3px; margin: 0.25rem 0; padding: 0; background-color: #dbdbdb;"></a>
                                        <c:if test="${sessionScope.user.role == 1}">
                                            <!--<a class="dropdown-item" href="ViewRequestController?requsername=${sessionScope.user.username}">Lịch sử yêu cầu</a>-->
                                        </c:if>
                                        <c:if test="${sessionScope.user.role == 2}">
                                            <a class="dropdown-item" href="view?id=listRequestOfMentee">Request History</a>
                                            <a class="dropdown-item" href="createRequest">Create request</a>
                                            <a class="dropdown-item" href="view?id=statisticRequest">Statistic Request</a>
                                            <a class="dropdown-item" href="updateProfile">Update Profile</a>
                                        </c:if>
                                        <c:if test="${sessionScope.user.role == 3}">
                                            <a class="dropdown-item" href="view?id=statisticRequestOfMentor">Statistic Request</a>
                                            <a class="dropdown-item" href="createCV">Create CV</a>
                                            <a class="dropdown-item" href="updateMentor">Update CV Of Mentor</a>
                                            <a class="dropdown-item" href="viewl">View list request</a>
                                            <a class="dropdown-item" href="ViewCVofMentor">View CV</a>
                                        </c:if>
                                        <c:if test="${sessionScope.user.role == 4}">
                                            <a class="dropdown-item" href="admin?action=createSkill">Create Skill</a>
                                            <a class="dropdown-item" href="admin?action=statisticMentee">Statistic Mentee</a>
                                            <a class="dropdown-item" href="adminRequest">List of Request</a>
                                            <a class="dropdown-item" href="ViewAllMentor">View all mentor</a>
                                            <a class="dropdown-item" href="admin?action=viewSkill">Skill Management</a>
                                        </c:if>
                                        <c:if test="${sessionScope.user.role != 1}">
                                            <a class="dropdown-item" style="height: 3px; margin: 0.25rem 0; padding: 0; background-color: #dbdbdb;"></a>
                                        </c:if>
                                        <a class="dropdown-item" href="logout">Logout</a>
                                    </div>
                                </li>
                            </c:if>     

                            <c:if test="${sessionScope.user == null}">
                                <li><a href="login">Login</a></li>
                                </c:if>
                        </ul>

                    </div>
                </div>
            </nav>
            <!--                        <div class="search-input" id="search-input-box">
                                        <div class="container">
                                            <form class="d-flex justify-content-between">
                                                <input type="text" class="form-control" id="search-input" placeholder="Search Here" />
                                                <button type="submit" class="btn"></button>
                                                <span class="lnr lnr-cross" id="close-search" title="Close Search"></span>
                                            </form>
                                        </div>
                                    </div>-->
        </header>

        <section class="home-banner-area">
            <div class="container">
                <div class="row justify-content-center fullscreen align-items-center">
                    <div class="col-lg-5 col-md-8 home-banner-left">
                        <h1 class="text-white">
                            Take the first step <br />
                            to learn with us
                        </h1>
                        <p class="mx-auto text-white  mt-20 mb-40">
                            In the history of modern astronomy, there is probably no one
                            greater leap forward than the building and launch of the space
                            telescope known as the Hubble.
                        </p>
                    </div>
                    <div class="offset-lg-2 col-lg-5 col-md-12 home-banner-right">
                        <img class="img-fluid" src="img/header-img.png" alt="" />
                    </div>
                </div>
            </div>
        </section>



        <div class="p-10 bg-surface-secondary">
            <div class="container">
                <div class="card">
                    <div class="card-header">
                        <form action="ViewAllMentor" method="Get">
                            <input type="text"  name="name" placeholder="Search" style="background-color:#e9e9e9">
                            <input type="submit" value="Search">
                        </form>
                    </div>
                    <c:if test="${sessionScope.name == null}">
                        <div class="table-responsive">
                            <table class="table table-hover table-nowrap col-md-8">
                                <thead class="table-light">
                                    <tr>
                                        <th scope="col">Name</th>
                                        <th scope="col">Username</th>
                                        <th scope="col">Profession</th>                                   
                                        <th scope="col">Star</th>             
                                        <th></th>
                                    </tr>
                                </thead>
                                <c:forEach var="o" items="${listMentorP}">
                                    <tbody>
                                        <tr>
                                            <td data-label="Name">
                                                <span>${o.name}</span>                              
                                            </td>
                                            <td data-label="Username">
                                                <span>${o.username}</span>
                                            </td>
                                            <td data-label="Profession">
                                                <span>${o.profession}</span>
                                            </td>
                                            <td data-label="Star">
                                                <div class="container-star">
                                                    <div class="rating-wrap">                                                       
                                                        <div class="center">
                                                            <fieldset class="rating">
                                                                <input type="radio" id="star5" name="rating" value="5"/><label for="star5" class="full" title="Awesome"></label>
                                                                <input type="radio" id="star4.5" name="rating" value="4.5"/><label for="star4.5" class="half"></label>
                                                                <input type="radio" id="star4" name="rating" value="4"/><label for="star4" class="full"></label>
                                                                <input type="radio" id="star3.5" name="rating" value="3.5"/><label for="star3.5" class="half"></label>
                                                                <input type="radio" id="star3" name="rating" value="3"/><label for="star3" class="full"></label>
                                                                <input type="radio" id="star2.5" name="rating" value="2.5"/><label for="star2.5" class="half"></label>
                                                                <input type="radio" id="star2" name="rating" value="2"/><label for="star2" class="full"></label>
                                                                <input type="radio" id="star1.5" name="rating" value="1.5"/><label for="star1.5" class="half"></label>
                                                                <input type="radio" id="star1" name="rating" value="1"/><label for="star1" class="full"></label>
                                                                <input type="radio" id="star0.5" name="rating" value="0.5"/><label for="star0.5" class="half"></label>
                                                            </fieldset>
                                                        </div>

                                                        <h4 id="rating-value"></h4>
                                                    </div>
                                                </div>

                                            </td>
                                        </tr>
                                    </tbody>
                                </c:forEach>      
                            </table>
                            <table class="table table-hover table-nowrap">                               
                                <thead class="table-light">
                                    <tr>
                                        <th scope="col">Number of Accepted</th>                                
                                </thead>
                                <c:forEach var="x" items="${accept}">
                                    <tbody>
                                        <tr>
                                            <td data-label="Number of Accepted">
                                                <span class="badge bg-soft-success text-success">${x}</span>
                                            </td>   
                                        </tr>
                                    </tbody>
                                </c:forEach>
                            </table>

                            <table class="table table-hover table-nowrap">
                                <thead class="table-light">
                                    <tr>
                                        <th scope="col">Number of  Canceled</th>                                
                                </thead>
                                <c:forEach var="y" items="${cancel}">
                                    <tbody>

                                        <tr>
                                            <td data-label="Number of Canceled">
                                                <span class="badge bg-soft-danger text-danger">${y}</span>
                                            </td>
                                        </tr>

                                    </tbody>
                                </c:forEach>
                            </table>
                            <table class="table table-hover table-nowrap">
                                <thead class="table-light">
                                    <tr>
                                        <th scope="col">Number of  Completed</th>                                
                                </thead>
                                <c:forEach var="z" items="${complete}">
                                    <tbody>
                                        <tr>
                                            <td data-label="Percentage Completed">
                                                <span class="badge bg-soft-success text-success">${z}%</span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </c:forEach>
                            </table>
                        </div>

                        <ul class="pagination">                  
                            <c:forEach begin="1" end="${endMentorP}" var="x">
                                <li class="page-item"><a class="page-link" href="ViewAllMentor?index=${x}">${x}</a></li>
                                </c:forEach>                                   
                        </ul>
                    </c:if>
                    <c:if test="${sessionScope.name != null}">
                        <div class="table-responsive">
                            <table class="table table-hover table-nowrap">
                                <thead class="table-light">
                                    <tr>
                                        <th scope="col">Name</th>
                                        <th scope="col">Username</th>
                                        <th scope="col">Profession</th>                                   
                                        <th scope="col">Star</th>             
                                        <th></th>
                                    </tr>
                                </thead>
                                <c:forEach var="o" items="${listP1}">
                                    <tbody>
                                        <tr>
                                            <td data-label="Name">
                                                <span>${o.name}</span>                              
                                            </td>
                                            <td data-label="Username">
                                                <span>${o.username}</span>
                                            </td>
                                            <td data-label="Profession">
                                                <span>${o.profession}</span>
                                            </td>
                                            <td data-label="Star">
                                                <div class="container-star">
                                                    <div class="rating-wrap">                                                       
                                                        <div class="center">
                                                            <fieldset class="rating">
                                                                <input type="radio" id="star5" name="rating" value="5"/><label for="star5" class="full" title="Awesome"></label>
                                                                <input type="radio" id="star4.5" name="rating" value="4.5"/><label for="star4.5" class="half"></label>
                                                                <input type="radio" id="star4" name="rating" value="4"/><label for="star4" class="full"></label>
                                                                <input type="radio" id="star3.5" name="rating" value="3.5"/><label for="star3.5" class="half"></label>
                                                                <input type="radio" id="star3" name="rating" value="3"/><label for="star3" class="full"></label>
                                                                <input type="radio" id="star2.5" name="rating" value="2.5"/><label for="star2.5" class="half"></label>
                                                                <input type="radio" id="star2" name="rating" value="2"/><label for="star2" class="full"></label>
                                                                <input type="radio" id="star1.5" name="rating" value="1.5"/><label for="star1.5" class="half"></label>
                                                                <input type="radio" id="star1" name="rating" value="1"/><label for="star1" class="full"></label>
                                                                <input type="radio" id="star0.5" name="rating" value="0.5"/><label for="star0.5" class="half"></label>
                                                            </fieldset>
                                                        </div>

                                                        <h4 id="rating-value"></h4>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </c:forEach>      
                            </table>
                            <table class="table table-hover table-nowrap  ">                               
                                <thead class="table-light">
                                    <tr>
                                        <th scope="col">Number of Accepted</th>                                
                                </thead>
                                <c:forEach var="x" items="${accept1}">
                                    <tbody>
                                        <tr>
                                            <td data-label="Number of Accepted">
                                                <span class="badge bg-soft-success text-success">${x}</span>
                                            </td>   
                                        </tr>
                                    </tbody>
                                </c:forEach>
                            </table>

                            <table class="table table-hover table-nowrap ">
                                <thead class="table-light">
                                    <tr>
                                        <th scope="col">Number of  Canceled</th>                                
                                </thead>
                                <c:forEach var="y" items="${cancel1}">
                                    <tbody>

                                        <tr>
                                            <td data-label="Number of Canceled">
                                                <span class="badge bg-soft-danger text-danger">${y}</span>
                                            </td>
                                        </tr>

                                    </tbody>
                                </c:forEach>
                            </table>
                            <table class="table table-hover table-nowrap ">
                                <thead class="table-light">
                                    <tr>
                                        <th scope="col">Number of  Completed</th>                                
                                </thead>
                                <c:forEach var="z" items="${complete1}">
                                    <tbody>
                                        <tr>
                                            <td data-label="Percentage Completed">
                                                <span class="badge bg-soft-success text-success">${z}%</span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </c:forEach>
                            </table>
                        </div>

                        <ul class="pagination">                  
                            <c:forEach begin="1" end="${endP1}" var="x">
                                <li class="page-item"><a class="page-link" href="ViewAllMentor?index=${x}&name=${sessionScope.name}">${x}</a></li>
                                </c:forEach>                                   
                        </ul>
                    </c:if>
                </div>
            </div>
        </div>

        <footer class="footer-area section-gap">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-md-6 single-footer-widget">
                        <h4>Top Products</h4>
                        <ul>
                            <li><a href="#">Managed Website</a></li>
                            <li><a href="#">Manage Reputation</a></li>
                            <li><a href="#">Power Tools</a></li>
                            <li><a href="#">Marketing Service</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-2 col-md-6 single-footer-widget">
                        <h4>Quick Links</h4>
                        <ul>
                            <li><a href="#">Jobs</a></li>
                            <li><a href="#">Brand Assets</a></li>
                            <li><a href="#">Investor Relations</a></li>
                            <li><a href="#">Terms of Service</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-2 col-md-6 single-footer-widget">
                        <h4>Features</h4>
                        <ul>
                            <li><a href="#">Jobs</a></li>
                            <li><a href="#">Brand Assets</a></li>
                            <li><a href="#">Investor Relations</a></li>
                            <li><a href="#">Terms of Service</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-2 col-md-6 single-footer-widget">
                        <h4>Resources</h4>
                        <ul>
                            <li><a href="#">Guides</a></li>
                            <li><a href="#">Research</a></li>
                            <li><a href="#">Experts</a></li>
                            <li><a href="#">Agencies</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-4 col-md-6 single-footer-widget">
                        <h4>Newsletter</h4>
                        <p>You can trust us. we only send promo offers,</p>
                        <div class="form-wrap" id="mc_embed_signup">
                            <form target="_blank" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01"
                                  method="get" class="form-inline">
                                <input class="form-control" name="EMAIL" placeholder="Your Email Address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Your Email Address '"
                                       required="" type="email">
                                <button class="click-btn btn btn-default text-uppercase">subscribe</button>
                                <div style="position: absolute; left: -5000px;">
                                    <input name="b_36c4fd991d266f23781ded980_aefe40901a" tabindex="-1" value="" type="text">
                                </div>

                                <div class="info"></div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="footer-bottom row align-items-center">
                    <p class="footer-text m-0 col-lg-8 col-md-12">
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
                    <div class="col-lg-4 col-md-12 footer-social">
                        <a href="#"><i class="fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                        <a href="#"><i class="fa fa-dribbble"></i></a>
                        <a href="#"><i class="fa fa-behance"></i></a>
                    </div>
                </div>
            </div>

        </footer>
        <script src="js/vendor/jquery-2.2.4.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
        crossorigin="anonymous"></script>
        <script src="js/vendor/bootstrap.min.js"></script>
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
        <script src="js/jquery.ajaxchimp.min.js"></script>
        <script src="js/jquery.magnific-popup.min.js"></script>
        <script src="js/parallax.min.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script src="js/jquery.sticky.js"></script>
        <script src="js/hexagons.min.js"></script>
        <script src="js/jquery.counterup.min.js"></script>
        <script src="js/waypoints.min.js"></script>
        <script src="js/jquery.nice-select.min.js"></script>
        <script src="js/main.js"></script>                         
    </body>
    <style>
        @import url(https://unpkg.com/@webpixels/css/dist/index.css);
        .table-responsive{
            display:flex;           
        }

        @import url(https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css);

        *{
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }

        html,body{
            width: 100%;
            height: 100%;
        }

        body{
            font-family: Arial, sans-serif;
        }

        .container-star{
            display: flex;
            align-items: center;
            justify-content: center;
            height: 100%;
        }

        .rating-wrap{
            max-width: 480px;
            margin: auto;
            text-align: center;
        }

        .center{
            width: 162px; 
            margin: auto;
        }


        #rating-value{	
            width: 110px;
            margin: 40px auto 0;
            padding: 10px 5px;
            text-align: center;
        }

        /*styling star rating*/
        .rating{
            border: none;
            float: left;
        }

        .rating > input{
            display: none;
        }

        .rating > label:before{
            content: '\f005';
            font-family: FontAwesome;
            margin: 5px;
            font-size: 1rem;
            display: inline-block;
            cursor: pointer;
        }

        .rating > .half:before{
            content: '\f089';
            position: absolute;
            cursor: pointer;
        }


        .rating > label{
            color: #ddd;
            float: right;
            cursor: pointer;
        }

        .rating > input:checked ~ label,
        .rating:not(:checked) > label:hover, 
        .rating:not(:checked) > label:hover ~ label{
            color: #f3e415;
        }

        .rating > input:checked + label:hover,
        .rating > input:checked ~ label:hover,
        .rating > label:hover ~ input:checked ~ label,
        .rating > input:checked ~ label:hover ~ label{
            color: #f3e415;
        }


    </style>
</html>
