
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Roboto'>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Manrope:wght@200&display=swap">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="https://fonts.googleapis.com/css?family=Playfair+Display:900|Roboto:400,400i,500,700" rel="stylesheet" />
        <!--
            CSS
            =============================================
        -->
        <link rel="stylesheet" href="css/linearicons.css" />
        <link rel="stylesheet" href="css/font-awesome.min.css" />
        <link rel="stylesheet" href="css/bootstrap.css" />
        <link rel="stylesheet" href="css/magnific-popup.css" />
        <link rel="stylesheet" href="css/owl.carousel.css" />
        <link rel="stylesheet" href="css/nice-select.css">
        <link rel="stylesheet" href="css/hexagons.min.css" />
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/themify-icons/0.1.2/css/themify-icons.css" />
        <link rel="stylesheet" href="css/main.css" />
        <link rel="stylesheet" href="https://unpkg.com/@webpixels/css/dist/index.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js">
        <style>
            html,body,h1,h2,h3,h4,h5,h6 {font-family: "Roboto", sans-serif}
            .card {
                position: relative;
                display: flex;
                flex-direction: column;
                min-width: 0;
                padding: 20px;
                width: 450px;
                word-wrap: break-word;
                background-color: #fff;
                background-clip: border-box;
                border-radius: 6px;
                -moz-box-shadow: 0px 0px 5px 0px rgba(212, 182, 212, 1)
            }

            .comment-box{

                padding:5px;
            }



            .comment-area textarea{
                resize: none; 
                border: 1px solid #ad9f9f;
            }


            .form-control:focus {
                color: #495057;
                background-color: #fff;
                border-color: #ffffff;
                outline: 0;
                box-shadow: 0 0 0 1px rgb(255, 0, 0) !important;
            }

            .send {
                color: #fff;
                background-color: #ff0000;
                border-color: #ff0000;
            }

            .send:hover {
                color: #fff;
                background-color: #f50202;
                border-color: #f50202;
            }


            .rating {
                display: flex;
                margin-top: -10px;
                flex-direction: row-reverse;
                margin-left: -4px;
                float: left;
            }

            .rating>input {
                display: none
            }

            .rating>label {
                position: relative;
                width: 19px;
                font-size: 25px;
                color: #ff0000;
                cursor: pointer;
            }

            .rating>label::before {
                content: "\2605";
                position: absolute;
                opacity: 0
            }

            .rating>label:hover:before,
            .rating>label:hover~label:before {
                opacity: 1 !important
            }

            .rating>input:checked~label:before {
                opacity: 1
            }

            .rating:hover>input:checked~label:before {
                opacity: 0.4
            }
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                padding-top: 100px; /* Location of the box */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            }

            /* Modal Content */
            .modal-content {
                background-color: #fefefe;
                margin: auto;
                padding: 20px;
                border: 1px solid #888;
                width: 80%;
            }

            /* The Close Button */
            .close {
                color: #aaaaaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

            .close:hover,
            .close:focus {
                color: #000;
                text-decoration: none;
                cursor: pointer;
            }
        </style>
    </head>
    <body class="w3-light-grey">
        <!-- Page Container -->
        <div class="w3-content w3-margin-top" style="max-width:1400px;">
            <h1><a href="home" style="text-decoration: none;color: blue">HOME</a> </h1>
            <p style="color: green">${messEvaluation}</p>
            <!-- The Grid -->
            <div class="w3-row-padding">
                <!-- Left Column -->
                <div class="w3-third">

                    <div class="w3-white w3-text-grey w3-card-4">
                        <div class="w3-display-container">
                            <img src="${sessionScope.mentor.img}" style="width:100%" alt="Avatar">
                            <!--                                <div class="w3-display-bottomleft w3-container w3-text-black">
                                                                <h2>${sessionScope.mentor.username}</h2>
                                                            </div>-->
                        </div>
                        <div class="w3-container">
                            <c:if test="${sessionScope.mentor.role == 1}">
                                <p><i class="fa fa-briefcase fa-fw w3-margin-right w3-large w3-text-teal"></i>Mentor</p>
                            </c:if>
                            <p><i class="fa fa-home fa-fw w3-margin-right w3-large w3-text-teal"></i>${sessionScope.mentor.address}</p>
                            <p><i class="fa fa-envelope fa-fw w3-margin-right w3-large w3-text-teal"></i>${sessionScope.mentor.email}</p>
                            <p><i class="fa fa-phone fa-fw w3-margin-right w3-large w3-text-teal"></i>${sessionScope.mentor.phone}</p>
                            <hr>

                            <!--                                <p class="w3-large"><b><i class="fa fa-asterisk fa-fw w3-margin-right w3-text-teal"></i>Rating Skill</b></p>
                                                            <p>Adobe Photoshop</p>
                                                            <div class="w3-light-grey w3-round-xlarge w3-small">
                                                                <div class="w3-container w3-center w3-round-xlarge w3-teal" style="width:90%">90%</div>
                                                            </div>
                                                            <p>Photography</p>
                                                            <div class="w3-light-grey w3-round-xlarge w3-small">
                                                                <div class="w3-container w3-center w3-round-xlarge w3-teal" style="width:80%">
                                                                    <div class="w3-center w3-text-white">80%</div>
                                                                </div>
                                                            </div>
                                                            <p>Illustrator</p>
                                                            <div class="w3-light-grey w3-round-xlarge w3-small">
                                                                <div class="w3-container w3-center w3-round-xlarge w3-teal" style="width:75%">75%</div>
                                                            </div>
                                                            <p>Media</p>
                                                            <div class="w3-light-grey w3-round-xlarge w3-small">
                                                                <div class="w3-container w3-center w3-round-xlarge w3-teal" style="width:50%">50%</div>
                                                            </div>-->
                            <!--<br>-->

                            <p class="w3-large"><b><i class="fa fa-asterisk fa-fw w3-margin-right w3-text-teal"></i> <button id="myBtn">Rating star</button></b></p>
<!--                                <p> 5 Star : ${sessionScope.u5star} voted</p>            
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>

                            <p> 4 Star : ${sessionScope.u4star} voted</p>            
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star "></span>

                            <p> 3 Star : ${sessionScope.u3star} voted</p>            
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star "></span>
                            <span class="fa fa-star "></span>

                            <p> 2 Star : ${sessionScope.u2star} voted</p>            
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star "></span>
                            <span class="fa fa-star "></span>
                            <span class="fa fa-star "></span>

                            <p> 1 Star : ${sessionScope.u1star} voted</p>            
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star "></span>
                            <span class="fa fa-star "></span>
                            <span class="fa fa-star "></span>
                            <span class="fa fa-star "></span>-->

                            <br>
                        </div>
                    </div><br>
                    <!-- End Left Column -->
                </div>
                <!-- Right Column -->
                <div class="w3-twothird">

                    <div class="w3-container w3-card w3-white w3-margin-bottom">
                        <h2 class="w3-text-grey w3-padding-16"><i class="fa fa-suitcase fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i>Service</h2>

                        <div class="w3-container">
                            <h5 class="w3-opacity" ><b></b></h5>
                            <p>${sessionScope.mentor.serviceDesc}</p>
                            <hr>
                        </div>
                    </div>
                    <div class="w3-container w3-card w3-white">
                        <h2 class="w3-text-grey w3-padding-16"><i class="fa fa-suitcase fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i>Job Introduction</h2>
                        <div class="w3-container">
                            <p style="color: black"> ${sessionScope.mentor.professionIntro}</p>
                            <hr>
                        </div>
                    </div>
                    <br>
                    <div class="w3-container w3-card w3-white">
                        <h2 class="w3-text-grey w3-padding-16"><i class="fa fa-certificate fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i>Achievement</h2>
                        <div class="w3-container">
                            <p>${sessionScope.mentor.archivementDesc}</p>
                            <hr>
                        </div>
                    </div>
                    <!-- End Right Column -->
                </div>
                <!-- End Grid -->
            </div>
            <!-- End Page Container -->
        </div>


        <!-- The Modal -->
        <div id="myModal" class="modal">

            <!-- Modal content -->
            <div class="modal-content">
                <span class="close">&times;</span>
                <div class="card">
                    <div class="row">
                        <div class="col-2">
                            <img src="${sessionScope.user.img}" width="70" class="rounded-circle mt-2">
                        </div>
                        <div class="col-10">
                            <div class="comment-box ml-2">
                                <h4>Add a comment</h4>
                                <form action="review" method="post">
                                    <input hidden="" name="action" value="review">
                                    <input hidden="" name="to" value="${sessionScope.mentor.username}">
                                    <div class="rating"> 
                                        <input type="radio" name="rating" value="5" id="5"><label for="5">&#10025;</label>
                                        <input type="radio" name="rating" value="4" id="4"><label for="4">&#10025;</label> 
                                        <input type="radio" name="rating" value="3" id="3"><label for="3">&#10025;</label>
                                        <input type="radio" name="rating" value="2" id="2"><label for="2">&#10025;</label>
                                        <input type="radio" name="rating" value="1" id="1"><label for="1">&#10025;</label>
                                    </div>
                                    <div class="comment-area">
                                        <textarea name="content" class="form-control" placeholder="What is your view?" rows="4"></textarea>
                                    </div>
                                    <div class="comment-btns mt-2">
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="pull-left">
                                                    <!--<button class="btn btn-success btn-sm">Cancel</button>-->      
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="pull-right">
                                                    <button class="btn btn-success send btn-sm">Send <i class="fa fa-long-arrow-right ml-1"></i></button>      
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>



                            </div>

                        </div>


                    </div>

                </div>
            </div>

        </div>


        <footer class="w3-container w3-teal w3-center w3-margin-top">
            <p>Find me on social media.</p>
            <i class="fa fa-facebook-official w3-hover-opacity"></i>
            <i class="fa fa-instagram w3-hover-opacity"></i>
            <i class="fa fa-snapchat w3-hover-opacity"></i>
            <i class="fa fa-pinterest-p w3-hover-opacity"></i>
            <i class="fa fa-twitter w3-hover-opacity"></i>
            <i class="fa fa-linkedin w3-hover-opacity"></i>
        </footer>


    </body>
    <script>
        // Get the modal
        var modal = document.getElementById("myModal");

// Get the button that opens the modal
        var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
        btn.onclick = function () {
            modal.style.display = "block";
        }

// When the user clicks on <span> (x), close the modal
        span.onclick = function () {
            modal.style.display = "none";
        }

// When the user clicks anywhere outside of the modal, close it
        window.onclick = function (event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
    </script>
    <style>
        .checked {
            color: orange;
        }
    </style>
</html>