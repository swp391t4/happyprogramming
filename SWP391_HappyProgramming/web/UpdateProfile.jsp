<%-- 
    Document   : CreateCVOfMentor
    Created on : Jun 14, 2022, 10:42:19 PM
    Author     : thuy huong
--%>

<%-- 
    Document   : Home
    Created on : May 26, 2022, 9:43:49 PM
    Author     : kingo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html lang="zxx" class="no-js">

    <head>
        <!-- Mobile Specific Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <!-- Favicon -->
        <link rel="shortcut icon" href="img/fav.png" />
        <!-- Author Meta -->
        <meta name="author" content="colorlib" />
        <!-- Meta Description -->
        <meta name="description" content="" />
        <!-- Meta Keyword -->
        <meta name="keywords" content="" />
        <!-- meta character set -->
        <meta charset="UTF-8" />
        <!-- Site Title -->

        <title>Eclipse Education</title>

        <title>Happy Programming</title>


        <link href="https://fonts.googleapis.com/css?family=Playfair+Display:900|Roboto:400,400i,500,700" rel="stylesheet" />
        <!--
            CSS
            =============================================
        -->
        <link rel="stylesheet" href="css/linearicons.css" />
        <link rel="stylesheet" href="css/font-awesome.min.css" />
        <link rel="stylesheet" href="css/bootstrap.css" />
        <link rel="stylesheet" href="css/magnific-popup.css" />
        <link rel="stylesheet" href="css/owl.carousel.css" />
        <link rel="stylesheet" href="css/nice-select.css">
        <link rel="stylesheet" href="css/hexagons.min.css" />
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/themify-icons/0.1.2/css/themify-icons.css" />
        <link rel="stylesheet" href="css/main.css" />

        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Creative CV</title>
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
        <link href="css/aos.css?ver=1.1.0" rel="stylesheet">
        <link href="css/bootstrap.min.css?ver=1.1.0" rel="stylesheet">
        <link href="css/main.css?ver=1.1.0" rel="stylesheet">
        <noscript>
        <style type="text/css">
            [data-aos] {
                opacity: 1 !important;
                transform: translate(0) scale(1) !important;
            }
        </style>
        </noscript>
    </head>

    <body>

        <!-- ================ Start Header Area ================= -->
        <header class="default-header">
            <nav class="navbar navbar-expand-lg  navbar-light">
                <div class="container">

                    <a class="navbar-brand" href="index.html">
                        <!--<img src="img/logo.png" alt="" />-->

                        <a class="navbar-brand" href="home">
                            <img style="width: 200px; height: auto" src="img/logo-6.png" alt="" />


                        </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="lnr lnr-menu"></span>
                        </button>

                        <div class="collapse navbar-collapse justify-content-end align-items-center" id="navbarSupportedContent">
                            <ul class="navbar-nav">
                            
                            <li> <a href="view?id=ViewAllSkill">View list of skills</a></li>
                            <!-- Dropdown -->
                            <c:if test="${sessionScope.user != null}">                          
                                <li class="dropdown">
                                    <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                                        <img style="height: 35px; width: 35px; margin-top: -5px" src="${sessionScope.user.img}" class="avatar avatar-sm rounded-circle me-2">
                                    </a>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item fa fa-user s_color" style="font-weight: bold" href="#">${sessionScope.user.name}</a>
                                        <a class="dropdown-item" style="height: 3px; margin: 0.25rem 0; padding: 0; background-color: #dbdbdb;"></a>
                                        <c:if test="${sessionScope.user.role == 1}">
                                            <!--<a class="dropdown-item" href="ViewRequestController?requsername=${sessionScope.user.username}">Lịch sử yêu cầu</a>-->
                                        </c:if>
                                        <c:if test="${sessionScope.user.role == 2}">
                                            <a class="dropdown-item" href="view?id=listRequestOfMentee">Request History</a>
                                            <a class="dropdown-item" href="createRequest">Create request</a>
                                            <a class="dropdown-item" href="view?id=statisticRequest">Statistic Request</a>
                                            <a class="dropdown-item" href="updateProfile">Update Profile</a>
                                        </c:if>
                                        <c:if test="${sessionScope.user.role == 3}">
                                            <a class="dropdown-item" href="view?id=statisticRequestOfMentor">Statistic Request</a>
                                            <a class="dropdown-item" href="createCV">Create CV</a>
                                            <a class="dropdown-item" href="updateMentor">Update CV Of Mentor</a>
                                            <a class="dropdown-item" href="viewl">View list request</a>
                                            <a class="dropdown-item" href="ViewCVofMentor">View CV</a>
                                        </c:if>
                                        <c:if test="${sessionScope.user.role == 4}">
                                            <a class="dropdown-item" href="admin?action=createSkill">Create Skill</a>
                                            <a class="dropdown-item" href="admin?action=statisticMentee">Statistic Mentee</a>
                                            <a class="dropdown-item" href="adminRequest">List of Request</a>
                                            <a class="dropdown-item" href="ViewAllMentor">View all mentor</a>
                                            <a class="dropdown-item" href="admin?action=viewSkill">Skill Management</a>
                                        </c:if>
                                        <c:if test="${sessionScope.user.role != 1}">
                                            <a class="dropdown-item" style="height: 3px; margin: 0.25rem 0; padding: 0; background-color: #dbdbdb;"></a>
                                        </c:if>
                                        <a class="dropdown-item" href="logout">Logout</a>
                                    </div>
                                </li>
                            </c:if>     

                            <c:if test="${sessionScope.user == null}">
                                <li><a href="login">Login</a></li>
                                </c:if>
                        </ul>
                        </div>
                </div>
            </nav>
            <div class="search-input" id="search-input-box">
                <div class="container">
                    <form class="d-flex justify-content-between">
                        <input type="text" class="form-control" id="search-input" placeholder="Search Here" />
                        <button type="submit" class="btn"></button>
                        <span class="lnr lnr-cross" id="close-search" title="Close Search"></span>
                    </form>
                </div>
            </div>
        </header>

        <!-- ================ End Header Area ================= -->

        <!-- ================ start banner Area ================= -->
        <section class="home-banner-area">
            <div class="container">
                <div class="row justify-content-center fullscreen align-items-center">
                    <div class="col-lg-5 col-md-8 home-banner-left">
                        <h1 class="text-white">
                            <!--                            Take the first step <br />
                                                        to learn with us-->
                        </h1>
                        <p class="mx-auto text-white  mt-20 mb-40">
                            <!--                            In the history of modern astronomy, there is probably no one
                                                        greater leap forward than the building and launch of the space
                                                        telescope known as the Hubble.-->
                        </p>
                    </div>
                    <div class="offset-lg-2 col-lg-5 col-md-12 home-banner-right">
                        <img class="img-fluid" src="img/header-img.png" alt="" />
                    </div>
                </div>
            </div>
        </section>
        <div class="page-content" >
            <b><h2  style="text-align: center; font-family: system-ui; font-size: xx-large;"> Update Profile</h2></b>
        </div>
        <div>

        </div>
        <div class="profile-page">
            <!--            <form class="login100-form validate-form" action="view" method="post">
                            <input type="hidden" name="id" value="createCvMentor">-->
            <div class="wrapper">
                <div class="page-header page-header-small" filter-color="green">
                    <div class="page-header-image" data-parallax="true" style="background-image: url('images/cc-bg-1.jpg')"></div>
                    <div class="container">
                        <form action="updateProfile" method="post">
                            <div class="row">
                                <p class="text-danger">${messs}</p>
                                <div class="col-lg-3 col-md-12">
                                    <div class="content-center">
                                        <div class="cc-profile-image"><a href="#"><img src="${mentorn.img}"width="200px" height="200px" alt="Image"/></a></div>
                                        <div class="h2 title">${mentorn.name}</div>
                                    </div>
                                </div>

                                <div class="col-lg-9 col-md-12">

                                    <div class="card-body">
                                        <div class="h4 mt-0 title"></div>
                                        <div class="row mt-3">
                                            <div class="col-sm-4"><strong class="text-uppercase">Account name:</strong></div>
                                            <div class="col-sm-8"><input type="text" name="username" value="${mentorn.username}" readonly="true" size="80" ></div>
                                        </div>
                                        <div class="row mt-3">
                                            <div class="col-sm-4"><strong class="text-uppercase">Full Name:</strong></div>
                                            <div class="col-sm-8"><input type="text" name="name" value="${mentorn.name}"size="80" required="required"></div>
                                        </div>
                                        <div class="row mt-3">
                                            <div class="col-sm-4"><strong class="text-uppercase">Sex:</strong></div>
                                            <div class="col-sm-8">
                                                <c:if test="${mentorn.gender == 1}">
                                                    Female <input class="input-checkbox100"  type="checkbox" checked name="gender" value="1" size="80">
                                                    Male <input class="input-checkbox100"  type="checkbox" name="gender" value="0" size="80">
                                                </c:if>
                                                <c:if test="${mentorn.gender == 0}">
                                                    Female <input class="input-checkbox100" type="checkbox"  name="gender" value="1" size="80" >
                                                    Male <input class="input-checkbox100"  type="checkbox" checked name="gender" value="0" size="80">
                                                </c:if>
                                            </div>
                                        </div>
                                        <div class="row mt-3">
                                            <div class="col-sm-4"><strong class="text-uppercase">Avatar:</strong></div>
                                            <div class="col-sm-8"><input type="file" name="img" accept="image/*" value="${mentorn.img}" size="80" required="required" ></div>
                                        </div>
                                        <div class="row mt-3">
                                            <div class="col-sm-4"><strong class="text-uppercase">Date of birth:</strong></div>
                                            <div class="col-sm-8"><input type="text" name="dob" value="${mentorn.DOB}" size="80" required="required" ></div>
                                        </div>
                                        <div class="row mt-3">
                                            <div class="col-sm-4"><strong class="text-uppercase">Email:</strong></div>
                                            <div class="col-sm-8"><input type="text" name="email" value="${mentorn.email}"size="80" readonly="true"></div>
                                        </div>
                                        <div class="row mt-3">
                                            <div class="col-sm-4"><strong class="text-uppercase">Phone:</strong></div>
                                            <div class="col-sm-8"><input type="text" name="phone" value="${mentorn.phone}"size="80"></div>
                                        </div>
                                        <div class="row mt-3">
                                            <div class="col-sm-4"><strong class="text-uppercase">Address:</strong></div>
                                            <div class="col-sm-8"><input type="text" name="address" value="${mentorn.address}" size="80" required="required"></div>
                                        </div>
  
                                    </div>

                                    <div class="container-login100-form-btn">
                                        <button style="background-color:  mediumorchid" type="submit" class="login100-form-btn,radius">
                                            Update Profile
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!--            </form>-->
</div>
<!-- ================ End banner Area ================= -->

<!-- ================ start footer Area ================= -->
<footer class="footer-area section-gap">
    <div class="container">
        <div class="row">
            <div class="col-lg-2 col-md-6 single-footer-widget">
                <h4>Top Products</h4>
                <ul>
                    <li><a href="#">Managed Website</a></li>
                    <li><a href="#">Manage Reputation</a></li>
                    <li><a href="#">Power Tools</a></li>
                    <li><a href="#">Marketing Service</a></li>
                </ul>
            </div>
            <div class="col-lg-2 col-md-6 single-footer-widget">
                <h4>Quick Links</h4>
                <ul>
                    <li><a href="#">Jobs</a></li>
                    <li><a href="#">Brand Assets</a></li>
                    <li><a href="#">Investor Relations</a></li>
                    <li><a href="#">Terms of Service</a></li>
                </ul>
            </div>
            <div class="col-lg-2 col-md-6 single-footer-widget">
                <h4>Features</h4>
                <ul>
                    <li><a href="#">Jobs</a></li>
                    <li><a href="#">Brand Assets</a></li>
                    <li><a href="#">Investor Relations</a></li>
                    <li><a href="#">Terms of Service</a></li>
                </ul>
            </div>
            <div class="col-lg-2 col-md-6 single-footer-widget">
                <h4>Resources</h4>
                <ul>
                    <li><a href="#">Guides</a></li>
                    <li><a href="#">Research</a></li>
                    <li><a href="#">Experts</a></li>
                    <li><a href="#">Agencies</a></li>
                </ul>
            </div>
            <div class="col-lg-4 col-md-6 single-footer-widget">
                <h4>Newsletter</h4>
                <p>You can trust us. we only send promo offers,</p>
                <div class="form-wrap" id="mc_embed_signup">
                    <form target="_blank" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01"
                          method="get" class="form-inline">
                        <input class="form-control" name="EMAIL" placeholder="Your Email Address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Your Email Address '"
                               required="" type="email">
                        <button class="click-btn btn btn-default text-uppercase">subscribe</button>
                        <div style="position: absolute; left: -5000px;">
                            <input name="b_36c4fd991d266f23781ded980_aefe40901a" tabindex="-1" value="" type="text">
                        </div>

                        <div class="info"></div>
                    </form>
                </div>
            </div>
        </div>
        <div class="footer-bottom row align-items-center">
            <p class="footer-text m-0 col-lg-8 col-md-12">
                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
            <div class="col-lg-4 col-md-12 footer-social">
                <a href="#"><i class="fa fa-facebook"></i></a>
                <a href="#"><i class="fa fa-twitter"></i></a>
                <a href="#"><i class="fa fa-dribbble"></i></a>
                <a href="#"><i class="fa fa-behance"></i></a>
            </div>
        </div>
    </div>
</footer>
<!-- ================ End footer Area ================= -->

<script src="js/vendor/jquery-2.2.4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
crossorigin="anonymous"></script>
<script src="js/vendor/bootstrap.min.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
<script src="js/jquery.ajaxchimp.min.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/parallax.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.sticky.js"></script>
<script src="js/hexagons.min.js"></script>
<script src="js/jquery.counterup.min.js"></script>
<script src="js/waypoints.min.js"></script>
<script src="js/jquery.nice-select.min.js"></script>
<script src="js/main.js"></script>

</body>

</html>

