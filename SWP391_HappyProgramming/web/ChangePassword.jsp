<%-- 
    Document   : ChangePassword
    Created on : May 22, 2022, 4:02:33 PM
    Author     : kingo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<link rel="stylesheet" href="css/logincss.css">
<!DOCTYPE html>

<html>
    <head>
        <title>Đổi mật khẩu</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--===============================================================================================-->	
        <link rel="icon" type="image/png" href="img/images_login/icons/favicon.ico"/>
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/vendor_login/bootstrap/css/bootstrap.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="fonts/fonts_login/font-awesome-4.7.0/css/font-awesome.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="fonts/fonts_login/Linearicons-Free-v1.0.0/icon-font.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/vendor_login/animate/animate.css">
        <!--===============================================================================================-->	
        <link rel="stylesheet" type="text/css" href="vendor/vendor_login/css-hamburgers/hamburgers.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/vendor_login/animsition/css/animsition.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/vendor_login/select2/select2.min.css">
        <!--===============================================================================================-->	
        <link rel="stylesheet" type="text/css" href="vendor/vendor_login/daterangepicker/daterangepicker.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="css/css_login/util.css">
        <link rel="stylesheet" type="text/css" href="css/css_login/main.css">
        <!--===============================================================================================-->
    </head>
    <body>

        <div class="limiter">
            <div class="container-login100">
                <div class="wrap-login100">
                    <div class="login100-form-title" style="background-image: url(https://cdn2.vectorstock.com/i/1000x1000/49/46/coding-and-programming-background-vector-19854946.jpg);">
                        <span class="login100-form-title-1">
                            <h1><a href="home" style="color: white; font-size: 2.5rem">Happy Programming</a></h1>
                            <h3>Đổi mật khẩu</h3>
                        </span>
                    </div>
                    <div>
                        <span style="color: red">${ms}</span>
                        <c:set var="test" value="${stt}"/>  
                        <c:if test="${test==1}">  
                            <span style="color: red">Đăng nhập <a href="login" style="color: blue; font-size: 2rem"> tại đây</a></span>  
                        </c:if>
                    </div>
                    <form class="login100-form validate-form" action="login" method="post">
                        <input type="hidden" name="go" value="changePassword">
                        <div><span id='message'></span></div>
                        <div class="wrap-input100 validate-input m-b-26" data-validate="Username is required">
                            <span class="label-input100">Tài khoản</span>
                            <input class="input100" type="text" name="username" placeholder="Tên đăng nhập" required="">
                            <span class="focus-input100"></span>
                        </div>

                        <div class="wrap-input100 validate-input m-b-18" data-validate = "Password is required">
                            <span class="label-input100">Mật khẩu cũ</span>
                            <input class="input100" type="password" name="oldPass" placeholder="Mật khẩu cũ" required="">
                            <span class="focus-input100"></span>
                        </div>
                        
                        <div class="wrap-input100 validate-input m-b-18" data-validate = "Password is required">
                            <span class="label-input100">Mật khẩu mới</span>
                            <input class="input100" type="password" id="password" name="newPass" placeholder="Mật khẩu mới" required="">
                            <span class="focus-input100"></span>
                        </div>
                        
                        <div class="wrap-input100 validate-input m-b-18" data-validate = "Password is required">
                            <span class="label-input100">Nhập lại mật khẩu</span>
                            <input class="input100" type="password" id="confirm_password" placeholder="Xác nhận mật khẩu" required="">
                            <span class="focus-input100"></span>
                        </div>

                        <!--<div><span id='message'></span></div>-->


                        <div class="container-login100-form-btn">
                            <input style="text-align: center; background-color: mediumorchid" type="submit" id="btn-ChangeP" class="login100-form-btn" name="submit" value="Đổi mật khẩu">
<!--                                Đổi mật khẩu
                            </button>-->
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!--===============================================================================================-->
        <script src="vendor/jquery/jquery-3.2.1.min.js"></script>
        <!--===============================================================================================-->
        <script src="vendor/animsition/js/animsition.min.js"></script>
        <!--===============================================================================================-->
        <script src="vendor/bootstrap/js/popper.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
        <!--===============================================================================================-->
        <script src="vendor/select2/select2.min.js"></script>
        <!--===============================================================================================-->
        <script src="vendor/daterangepicker/moment.min.js"></script>
        <script src="vendor/daterangepicker/daterangepicker.js"></script>
        <!--===============================================================================================-->
        <script src="vendor/countdowntime/countdowntime.js"></script>
        <!--===============================================================================================-->
        <script src="js/main.js"></script>

    </body>

    
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript">
        $('#password, #confirm_password, #username').on('keyup', function () {
            if ($('#password').val() === $('#confirm_password').val() && $('#password').val().length > 5 && $('#password').val().indexOf(" ") < 0) {
                $('#message').html('Hợp lệ').css('color', 'green');
                document.getElementById("btn-ChangeP").setAttribute("type", "submit");
            } else if ($('#password').val().length < 6) {
                $('#message').html('Mật khẩu phải có ít nhất 6 ký tự').css('color', 'red');
                document.getElementById("btn-ChangeP").setAttribute("type", "");
                document.getElementById("btn-ChangeP").setAttribute("readonly", "true");
            } else if($('#password').val() !== $('#confirm_password').val()){
                $('#message').html('Mật khẩu không trùng nhau').css('color', 'red');
                document.getElementById("btn-ChangeP").setAttribute("type", "");
                document.getElementById("btn-ChangeP").setAttribute("readonly", "true");
            } else if($('#password').val().indexOf(" ") >= 0){
                $('#message').html('Mật khẩu không được chứa khoảng trắng').css('color', 'red');
                document.getElementById("btn-ChangeP").setAttribute("type", "");
                document.getElementById("btn-ChangeP").setAttribute("readonly", "true");
            }
            ////else{
//                $('#message').html('Tài khoản phải có ít nhất 6 ký tự').css('color', 'red');
//                document.getElementById("btn-signUp").setAttribute("type", "");
//                document.getElementById("btn-signUp").setAttribute("readonly", "true");
//            }
        });
    </script>
</html>
