

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="zxx" class="no-js">

    <head>
        <!-- Mobile Specific Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <!-- Favicon -->
        <link rel="shortcut icon" href="img/fav.png" />
        <!-- Author Meta -->
        <meta name="author" content="colorlib" />
        <!-- Meta Description -->
        <meta name="description" content="" />
        <!-- Meta Keyword -->
        <meta name="keywords" content="" />
        <!-- meta character set -->
        <meta charset="UTF-8" />
        <!-- Site Title -->
        <title>Happy Programming</title>

        <link href="https://fonts.googleapis.com/css?family=Playfair+Display:900|Roboto:400,400i,500,700" rel="stylesheet" />
        <!--
            CSS
            =============================================
        -->
        <link rel="stylesheet" href="css/linearicons.css" />
        <link rel="stylesheet" href="css/font-awesome.min.css" />
        <link rel="stylesheet" href="css/bootstrap.css" />
        <link rel="stylesheet" href="css/magnific-popup.css" />
        <link rel="stylesheet" href="css/owl.carousel.css" />
        <link rel="stylesheet" href="css/nice-select.css">
        <link rel="stylesheet" href="css/hexagons.min.css" />
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/themify-icons/0.1.2/css/themify-icons.css" />
        <link rel="stylesheet" href="css/main.css" />
        <link rel="stylesheet" href="https://unpkg.com/@webpixels/css/dist/index.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css">
        <style>
            @import url('https://fonts.googleapis.com/css2?family=Poppins&display=swap');
            *{
                padding: 0;
                margin: 0;
                box-sizing: border-box;
                font-family: 'Poppins', sans-serif;
                font-size: 0.95rem;
            }
            body{
                background-color: #f3f3f3;
            }
            #starred{
                box-shadow: 3px 3px 10px #b5b5b5;
            }
            .table div.text-muted{
                font-size: 0.85rem;
                font-weight: 600;
                margin-bottom: 0.3rem;
                margin-top: 0.3rem;
            }
            .icons{
                object-fit: contain;
                width: 25px;
                height: 25px;
                border-radius: 50%;
            }
            .graph img{
                object-fit: contain;
                width: 40px;
                height: 50px;
                transform: scale(2) rotateY(45deg);
            }
            .graph .dot{
                width: 12px;
                height: 12px;
                border-radius: 50%;
                border: 3px solid #fff;
                position: absolute;
                background-color: blue;
                box-shadow: 1px 1px 1px #a5a5a5;
                top: 25px;
            }
            .graph .dot:after{
                background-color: #fff;
                content: '$9,999.00';
                font-weight: 600;
                font-size: 0.7rem;
                position: absolute;
                top: -25px;
                left: -20px;
                box-shadow: 1px 1px 2px #a5a5a5;
                border-radius: 2px;
            }
            .font-weight-bold{
                font-size: 1.3rem;

            }
            #ethereum{
                transform: scale(2) rotateY(45deg) rotateX(180deg);
            }
            #ripple{
                transform: scale(2) rotateY(10deg) rotateX(20deg);
            }
            #eos{
                transform: scale(2) rotateY(50deg) rotateX(190deg);
            }



            /* utility classes */
            .table tr td{
                border: none;
            }
            .red{
                color: #ff2f2f;
                font-weight: 700;
            }
            .green{
                color: #1cbb1c;
                font-weight: 700;
            }
            .labels,.graph{
                position: relative;
            }
            .green-label{
                background-color: #00b300;
                color: #fff;
                font-weight: 600;
                font-size: 0.7rem;
            }
            .orange-label{
                background-color: #ffa500;
                color: #fff;
                font-weight: 600;
                font-size: 0.7rem;
            }
            .border-right{
                transform: scale(0.6);
                border-right: 1px solid black!important;
            }
            .box{
                transform: scale(1.5);
                background-color: #dbe2ff;
            }
            #top .table tbody tr{
                border-bottom: 1px solid #ddd;
            }
            #top .table tbody tr:last-child{
                border: none;
            }
            select{
                background-color: inherit;
                padding: 8px;
                border-radius: 5px;
                color: #444;
                border: 1px solid #444;
                outline-color: #00f;
            }
            .text-white{
                background-color: rgb(43, 159, 226);
                border-radius: 50%;
                font-size: 0.7rem;
                font-weight: 700;
                padding: 2px 3px;
            }
            a:hover{
                text-decoration: none;
            }
            a:hover .text-white{
                background-color: rgb(20, 92, 187);
            }

            /* Scrollbars */
            ::-webkit-scrollbar{
                width: 10px;
                height: 4px;
            }
            ::-webkit-scrollbar-thumb{
                background: linear-gradient(45deg,#999,#777);
                border-radius: 10px;

            }

            /* media Queries */
            @media(max-width:379px){
                .d-lg-flex .h3{
                    font-size: 1.4rem;
                }
            }
            @media(max-width:352px){
                #plat{
                    margin-top: 10px;
                }
            }


            .text-heading {
                margin-bottom: 30px;
                font-size: .8125rem;
            }
            .ratings{
                margin-right:10px;
            }

            .ratings i{

                color:#cecece;
                font-size:32px;
            }

            .rating-color{
                color:#fbc634 !important;
            }
            .small-ratings i{
                color:#cecece;   
            }
        </style>
    </head>

    <body>
        <!-- ================ Start Header Area ================= -->
        <header class="default-header">
            <nav class="navbar navbar-expand-lg  navbar-light">
                <div class="container">
                    <a class="navbar-brand" href="home">
                        <img style="width: 200px; height: auto" src="img/logo-6.png" alt="" />
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="lnr lnr-menu"></span>
                    </button>

                    <div class="collapse navbar-collapse justify-content-end align-items-center" id="navbarSupportedContent">
                        <ul class="navbar-nav">
                            <li> <a href="view?id=ViewAllSkill" style="text-decoration: none;">View list of skills</a></li>
                            <!-- Dropdown -->
                            <c:if test="${sessionScope.user != null}">                          
                                <li class="dropdown">
                                    <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                                        <img style="height: 35px; width: 35px; margin-top: -5px" src="${sessionScope.user.img}" class="avatar avatar-sm rounded-circle me-2">
                                    </a>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item fa fa-user s_color" style="font-weight: bold" href="#">${sessionScope.user.name}</a>
                                        <a class="dropdown-item" style="height: 3px; margin: 0.25rem 0; padding: 0; background-color: #dbdbdb;"></a>
                                        <c:if test="${sessionScope.user.role == 1}">
                                            <!--<a class="dropdown-item" href="ViewRequestController?requsername=${sessionScope.user.username}">Lịch sử yêu cầu</a>-->
                                        </c:if>
                                        <c:if test="${sessionScope.user.role == 2}">
                                            <a class="dropdown-item" href="view?id=listRequestOfMentee">Request History</a>
                                            <a class="dropdown-item" href="createRequest">Create request</a>
                                            <a class="dropdown-item" href="view?id=statisticRequest">Statistic Request</a>
                                        </c:if>
                                        <c:if test="${sessionScope.user.role == 3}">
                                            <a class="dropdown-item" href="view?id=statisticRequestOfMentor">Statistic Request</a>
                                            <a class="dropdown-item" href="createCV">Create CV</a>
                                            <a class="dropdown-item" href="viewl">View list request</a>
                                        </c:if>


                                        <c:if test="${sessionScope.user.role == 4}">
                                            <a class="dropdown-item" href="admin">Create Skill</a>
                                            <a class="dropdown-item" href="course-details.html">Course Details</a>
                                        </c:if>
                                        <c:if test="${sessionScope.user.role != 1}">
                                            <a class="dropdown-item" style="height: 3px; margin: 0.25rem 0; padding: 0; background-color: #dbdbdb;"></a>
                                        </c:if>
                                        <a class="dropdown-item" style="text-decoration: none;" href="logout">Logout</a>
                                    </div>
                                </li>
                            </c:if>     

                               

                    <c:if test="${sessionScope.user == null}">
                        <li><a href="login">Login</a></li>
                        </c:if>
                    </ul>
                </div>
                </div>
            </nav>
            <!--            <div class="search-input" id="search-input-box">
                            <div class="container">
                                <form class="d-flex justify-content-between">
                                    <input type="text" class="form-control" id="search-input" placeholder="Search Here" />
                                    <button type="submit" class="btn"></button>
                                    <span class="lnr lnr-cross" id="close-search" title="Close Search"></span>
                                </form>
                            </div>
                        </div>-->
        </header>
        <!-- ================ End Header Area ================= -->

        <!-- ================ start banner Area ================= -->
        <section class="home-banner-area">
            <div class="container">
                <div style="max-height: 440px;" class="row justify-content-center fullscreen align-items-center">
                    <div class="col-lg-5 col-md-8 home-banner-left">
                        <h1 class="text-white">
                            Take the first step <br />
                            to learn with us
                        </h1><!--
                        <p class="mx-auto text-white  mt-20 mb-40">
                            In the history of modern astronomy, there is probably no one
                            greater leap forward than the building and launch of the space
                            telescope known as the Hubble.
                        </p>-->
                    </div>
                    <div class="offset-lg-2 col-lg-5 col-md-12 home-banner-right">
                        <img class="img-fluid" src="img/header-img.png" alt="" />
                    </div>
                </div>
            </div>
        </section>
        <!-- ================ End banner Area ================= -->

        <!-- ================ Start Feature Area ================= -->
        <section class="feature-area" style="margin-top: 1.5rem">

            <br>
            <br>
            <br>
            <div class="container mt-5">
                <div class="h3 text-muted">Thống kê</div>
                <div id="starred" class="bg-white px-2 pt-1 mt-2">
                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td>
                                        <div class="d-flex mt-2 border-right">
                                            <div class="box p-2 rounded">
                                                <span class="fas fa-star text-primary px-1"></span>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="d-flex flex-column">
                                            <div class="text-muted">Tổng số yêu cầu</div>
                                            <div class="d-flex align-items-center">

                                                <b class="pl-2">${rtotal}</b>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="d-flex flex-column">
                                            <div class="text-muted">Tổng thời gian yêu cầu</div>
                                            <div><b>${rtotalh}</b></div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="d-flex flex-column">
                                            <div class="text-muted">Tổng số mentor</div>
                                            <div><b>${rtotalm}</b></div>
                                        </div>
                                    </td>

                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <br>
                <br>
                <div class="h3 text-muted">Danh sách các yêu cầu</div>
                <div id="top">
                    <div class="bg-white table-responsive">
                        <table class="table">
                            <tbody>
                                <c:forEach items="${list}" var="l" varStatus="i">
                                    <tr>
                                        <td>
                                            <div class="d-flex mt-2 border-right">
                                                <div class="box p-2 rounded">
                                                    <span class="text-primary px-2 font-weight-bold">${i.index+1}</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex flex-column">
                                                <!--<div class="text-muted">Từ</div>-->
                                                <div class="d-flex align-items-center">

                                                    <b class="pl-2">${l.from}</b>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex flex-column">
                                                <!--<div class="text-muted">Đến</div>-->
                                                <div><b>${l.to}</b></div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex flex-column">
                                                <!--<div class="text-muted">Nội dung</div>-->
                                                <div><b>${l.title}</b></div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex flex-column">
                                                <div class="d-flex align-items-center labels">
                                                    <!--<div class="text-muted">Ngày gửi yêu cầu</div>-->
                                                </div>
                                                <div><b>${l.dateRe}</b></div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex flex-column">
                                                <div class="d-flex align-items-center labels">
                                                    <!--<div class="text-muted">Hạn</div>-->
                                                </div>
                                                <div><b class="red">${l.deadlineDate}</b></div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex flex-column">
                                                <div class="d-flex align-items-center labels">
                                                    <!--<div class="text-muted">Số giờ</div>-->
                                                </div>
                                                <div><b class="red">${l.deadHour}</b></div>
                                            </div>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <br>
    <br>
    <br>
    <br>

    <!--            <div class="container-fluid">
                    Banner 
                    <a href="https://webpixels.io/components?ref=codepen" class="btn w-full btn-primary text-truncate rounded-0 py-2 border-0 position-relative" style="z-index: 1000;">
                        <strong>Crafted with Webpixels CSS:</strong> The design system for Bootstrap 5. Browse all components &rarr;
                    </a>
    
    
    
                </div>-->
</section>
<!--        <section class="feature-area" style="margin-top: 1.5rem">
            abcabshdasd
        </section>-->
<!-- ================ End Feature Area ================= -->

<!-- ================ Start Popular Course Area ================= -->

<!-- ================ End Registration Area ================= -->

<!-- ================ Start Blog Post Area ================= -->
<!--        <section class="blog-post-area section-gap">
            
        </section>-->
<!-- ================ End Blog Post Area ================= -->

<!-- ================ start footer Area ================= -->
<footer class="footer-area section-gap">
    <div class="container">
        <div class="row">
            <div class="col-lg-2 col-md-6 single-footer-widget">
                <h4>Top Products</h4>
                <ul>
                    <li><a href="#">Managed Website</a></li>
                    <li><a href="#">Manage Reputation</a></li>
                    <li><a href="#">Power Tools</a></li>
                    <li><a href="#">Marketing Service</a></li>
                </ul>
            </div>
            <div class="col-lg-2 col-md-6 single-footer-widget">
                <h4>Quick Links</h4>
                <ul>
                    <li><a href="#">Jobs</a></li>
                    <li><a href="#">Brand Assets</a></li>
                    <li><a href="#">Investor Relations</a></li>
                    <li><a href="#">Terms of Service</a></li>
                </ul>
            </div>
            <div class="col-lg-2 col-md-6 single-footer-widget">
                <h4>Features</h4>
                <ul>
                    <li><a href="#">Jobs</a></li>
                    <li><a href="#">Brand Assets</a></li>
                    <li><a href="#">Investor Relations</a></li>
                    <li><a href="#">Terms of Service</a></li>
                </ul>
            </div>
            <div class="col-lg-2 col-md-6 single-footer-widget">
                <h4>Resources</h4>
                <ul>
                    <li><a href="#">Guides</a></li>
                    <li><a href="#">Research</a></li>
                    <li><a href="#">Experts</a></li>
                    <li><a href="#">Agencies</a></li>
                </ul>
            </div>
            <div class="col-lg-4 col-md-6 single-footer-widget">
                <h4>Newsletter</h4>
                <p>You can trust us. we only send promo offers,</p>
                <div class="form-wrap" id="mc_embed_signup">
                    <form target="_blank" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01"
                          method="get" class="form-inline">
                        <input class="form-control" name="EMAIL" placeholder="Your Email Address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Your Email Address '"
                               required="" type="email">
                        <button class="click-btn btn btn-default text-uppercase">subscribe</button>
                        <div style="position: absolute; left: -5000px;">
                            <input name="b_36c4fd991d266f23781ded980_aefe40901a" tabindex="-1" value="" type="text">
                        </div>

                        <div class="info"></div>
                    </form>
                </div>
            </div>
        </div>
        <div class="footer-bottom row align-items-center">
            <p class="footer-text m-0 col-lg-8 col-md-12">
                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
            <div class="col-lg-4 col-md-12 footer-social">
                <a href="#"><i class="fa fa-facebook"></i></a>
                <a href="#"><i class="fa fa-twitter"></i></a>
                <a href="#"><i class="fa fa-dribbble"></i></a>
                <a href="#"><i class="fa fa-behance"></i></a>
            </div>
        </div>
    </div>
</footer>
<!-- ================ End footer Area ================= -->

<script src="js/vendor/jquery-2.2.4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
crossorigin="anonymous"></script>
<script src="js/vendor/bootstrap.min.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
<script src="js/jquery.ajaxchimp.min.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/parallax.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.sticky.js"></script>
<script src="js/hexagons.min.js"></script>
<script src="js/jquery.counterup.min.js"></script>
<script src="js/waypoints.min.js"></script>
<script src="js/jquery.nice-select.min.js"></script>
<script src="js/main.js"></script>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script type="text/javascript">
                    function Page(index, totalP) {
                        $.ajax({
                            url: "/SWP391_HappyProgramming/view",
                            type: "get", //send it through get method
                            data: {
                                id: 'listPageSkill',

                                index: index,
                                totalP: totalP
                            },
                            success: function (response) {
                                //Do Something
                                document.getElementById("page").innerHTML = response;

                            },
                            error: function (xhr) {
                                //Do Something to handle error
                            }
                        });
                    }

                    function Pagination(index, totalP) {
                        $.ajax({
                            url: "/SWP391_HappyProgramming/view",
                            type: "get", //send it through get method
                            data: {
                                id: 'paginationListSkill',
                                index: index
                            },
                            success: function (response) {
                                //Do Something
                                document.getElementById("data").innerHTML = response;
                                Page(index, totalP);
                            },
                            error: function (xhr) {
                                //Do Something to handle error
                            }
                        });
                    }
</script>

</html>



