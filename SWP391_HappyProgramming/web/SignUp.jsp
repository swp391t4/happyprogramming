
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>


<link rel="stylesheet" href="css/logincss.css">

<!DOCTYPE html>

<html>
    <head>
        <title>Đăng ký</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--===============================================================================================-->	
        <link rel="icon" type="image/png" href="image/images_login/icons/favicon.ico"/>
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/vendor_login/bootstrap/css/bootstrap.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="fonts/fonts_login/font-awesome-4.7.0/css/font-awesome.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="fonts/fonts_login/Linearicons-Free-v1.0.0/icon-font.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/vendor_login/animate/animate.css">
        <!--===============================================================================================-->	
        <link rel="stylesheet" type="text/css" href="vendor/vendor_login/css-hamburgers/hamburgers.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/vendor_login/animsition/css/animsition.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/vendor_login/select2/select2.min.css">
        <!--===============================================================================================-->	
        <link rel="stylesheet" type="text/css" href="vendor/vendor_login/daterangepicker/daterangepicker.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="css/css_login/util.css">
        <link rel="stylesheet" type="text/css" href="css/css_login/main.css">
        <!--===============================================================================================-->
    </head>
    <body>

        <div class="limiter">
            <div class="container-login100">
                <div class="wrap-login100">
                    <div class="login100-form-title" style="background-image: url(https://cdn2.vectorstock.com/i/1000x1000/49/46/coding-and-programming-background-vector-19854946.jpg);">
                        <span class="login100-form-title-1">
                            <h1><a href="home" style="color: white; font-size: 2.5rem">Happy Programming</a></h1>
                            <h3>Đăng ký</h3>
                        </span>
                    </div>
                    <div style="text-align: center"><span style="color: red; text-align: center">${ms}</span></div>
                    <div style="text-align: center"><span style="color: red; text-align: center">${mes}</span></div>
                    <div style="text-align: center"><span style="color: red; text-align: center">${q}</span></div>
                    <form class="login100-form validate-form" action="login" method="post" onsubmit="return checkForm(this);">
                        <input type="hidden" name="go" value="signup">
                        <div><span id='message'></span></div>
                        <div class="wrap-input100 validate-input m-b-26" data-validate="Username is required">
                            <span class="label-input100">Tài khoản</span>
                            <input class="input100" type="text" id="username" name="username" placeholder="Tên đăng nhập" value="${username}">
                            <span class="focus-input100"></span>
                        </div>

                        <div class="wrap-input100 validate-input m-b-18" data-validate = "Password is required">
                            <span class="label-input100">Mật khẩu </span>
                            <input class="input100" type="password" id="password" name="password" placeholder="Mật khẩu" value="${password}">
                            <span class="focus-input100"></span>
                        </div>

                        <div class="wrap-input100 validate-input m-b-18" data-validate = "Password is required">
                            <span class="label-input100">Xác nhận lại mật khẩu </span>
                            <input class="input100" type="password"  id="confirm_password" name="repassword" placeholder="Xác nhận mật khẩu " value="${repassword}">
                            <span class="focus-input100"></span>
                        </div>

                        <div class="wrap-input100 validate-input m-b-18" data-validate = "Password is required">
                            <span class="label-input100">Họ và tên </span>
                            <input class="input100" type="text"  id="confirm_password" name="name" value="${name}" placeholder="Nhập tên của bạn" >
                            <span class="focus-input100"></span>
                        </div>

                        <div class="wrap-input100 validate-input m-b-18" data-validate = "Password is required">
                            <span class="label-input100">Email</span>
                            <input class="input100" type="email" id="c_email" name="email" placeholder="Địa chỉ Email" value="${email}">
                            <span class="focus-input100"></span>
                        </div>

                        <div class="wrap-input100 validate-input m-b-18" data-validate = "Password is required">
                            <span class="label-input100">Số điện thoại</span>
                            <input class="input100" type="text" id="c_phone" name="phone" placeholder="Số điện thoại" value="${phone}">
                            <span class="focus-input100"></span>
                        </div>

                        <div class="wrap-input100 validate-input m-b-18" data-validate = "Password is required">
                            <span class="label-input100">Địa chỉ</span>
                            <input class="input100" type="text" name="address" placeholder="Nhập địa chỉ" value="${address}" >
                            <span class="focus-input100"></span>
                        </div>

                        <div class="wrap-input100 validate-input m-b-18" data-validate = "Password is required">
                            <span class="label-input100">Ngày sinh</span>
                            <input class="input100"  type="date" name="dob" >
                            <span class="focus-input100"></span>
                        </div>

                        <div style="text-align: center; padding-bottom: 14px" class="wrap-input100 validate-input m-b-18" data-validate = "Password is required">
                            <span class="label-input100">Giới tính</span>
                            <input  style="width: 10%;" type="radio" name="gender" value="1">Nam
                            <input  style="width: 10%" type="radio" name="gender" value="0">Nữ
                            <span class="focus-input100"></span>
                        </div>

                        <div style="text-align: center; padding-bottom: 14px" class="wrap-input100 validate-input m-b-18" data-validate = "Password is required">
                            <span class="label-input100">Chọn vai trò</span>
                            <input style="width: 10%;" type="radio" name="role" value="1">Member
                            <input style="width: 10%;" type="radio" name="role" value="2">Mentee
                            <input style="width: 10%;" type="radio" name="role" value="3">Mentor
                            <span class="focus-input100"></span>
                        </div>

                        <!--<div><span id='message'></span></div>-->


                        <div class="container-login100-form-btn">
                            <input style="text-align: center; background-color: mediumorchid" type="submit" id="btn-ChangeP" class="login100-form-btn" name="submit" value="Đăng Ký">
                            <!--                                Đổi mật khẩu
                                                        </button>-->
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!--===============================================================================================-->
        <script src="vendor/jquery/jquery-3.2.1.min.js"></script>
        <!--===============================================================================================-->
        <script src="vendor/animsition/js/animsition.min.js"></script>
        <!--===============================================================================================-->
        <script src="vendor/bootstrap/js/popper.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
        <!--===============================================================================================-->
        <script src="vendor/select2/select2.min.js"></script>
        <!--===============================================================================================-->
        <script src="vendor/daterangepicker/moment.min.js"></script>
        <script src="vendor/daterangepicker/daterangepicker.js"></script>
        <!--===============================================================================================-->
        <script src="vendor/countdowntime/countdowntime.js"></script>
        <!--===============================================================================================-->
        <script src="js/main.js"></script>

    </body>


    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript">
                        $('#password, #confirm_password, #username, #c_email, #c_phone').on('keyup', function () {
                            var email = new RegExp('[a-zA-Z]\\w+@\\w+(\\.\\w+){1,3}');
                            var phone = new RegExp('(\\+[0-9]{1,2})?[0-9]{8,11}');
//                            -if ($('#password').val() === $('#confirm_password').val() && $('#password').val().length > 5 && $('#password').val().indexOf(" ") < 0 && email.test($('#c_email').val()) && phone.test($('#c_phone').val())) {

                            if ($('#password').val() === $('#confirm_password').val() && $('#password').val().length > 5 && $('#password').val().indexOf(" ") < 0 && $('#username').val().length > 5 && $('#username').val().indexOf(" ") < 0 && email.test($('#c_email').val()) && phone.test($('#c_phone').val())) {
                                $('#message').html('Hợp lệ').css('color', 'green');
                                document.getElementById("btn-ChangeP").setAttribute("type", "submit");
                            } else if ($('#username').val().length < 6) {
                                $('#message').html('Tài khoản phải có ít nhất 6 ký tự').css('color', 'red');
                                document.getElementById("btn-signUp").setAttribute("type", "");
                                document.getElementById("btn-signUp").setAttribute("readonly", "true");
                            }else if ($('#username').val().indexOf(" ") > 0){
                                $('#message').html('Tài khoản không được chứa dấu cách').css('color', 'red');
                                document.getElementById("btn-signUp").setAttribute("type", "");
                                document.getElementById("btn-signUp").setAttribute("readonly", "true");
                            } else if ($('#password').val().length < 6) {
                                $('#message').html('Mật khẩu phải có ít nhất 6 ký tự').css('color', 'red');
                                document.getElementById("btn-ChangeP").setAttribute("type", "");
                                document.getElementById("btn-ChangeP").setAttribute("readonly", "true");
                            } else if ($('#password').val() !== $('#confirm_password').val()) {
                                $('#message').html('Mật khẩu không trùng nhau').css('color', 'red');
                                document.getElementById("btn-ChangeP").setAttribute("type", "");
                                document.getElementById("btn-ChangeP").setAttribute("readonly", "true");
                            } else if ($('#password').val().indexOf(" ") >= 0) {
                                $('#message').html('Mật khẩu không được chứa khoảng trắng').css('color', 'red');
                                document.getElementById("btn-ChangeP").setAttribute("type", "");
                                document.getElementById("btn-ChangeP").setAttribute("readonly", "true");
                            } else if (!email.test($('#c_email').val())) {
                                $('#message').html('Email không đúng định dạng').css('color', 'red');
                                document.getElementById("btn-ChangeP").setAttribute("type", "");
                                document.getElementById("btn-ChangeP").setAttribute("readonly", "true");
                            } else if (!phone.test($('#c_phone').val())) {
                                $('#message').html('Số điện thoại không đúng định dạng').css('color', 'red');
                                document.getElementById("btn-ChangeP").setAttribute("type", "");
                                document.getElementById("btn-ChangeP").setAttribute("readonly", "true");
                            }
                            
                            
                        });
//                        function checkForm(form)
//                        {
//                            if (form.password.value != "" && form.password.value == form.repassword.value) {
//                                if (form.password.value.length < 6) {
//                                    alert("Error: Password must contain at least 6 characters!");
//                                    form.password.focus();
//                                    return false;
//                                }
//                                re = /[0-9]/;
//                                if (!re.test(form.password.value)) {
//                                    alert("Error: password must contain at least one number (0-9)!");
//                                    form.password.focus();
//                                    return false;
//                                }
//                                re = /[a-z]/;
//                                if (!re.test(form.password.value)) {
//                                    alert("Error: password must contain at least one lowercase letter (a-z)!");
//                                    form.password.focus();
//                                    return false;
//                                }
//                                re = /[A-Z]/;
//                                if (!re.test(form.password.value)) {
//                                    alert("Error: password must contain at least one uppercase letter (A-Z)!");
//                                    form.password.focus();
//                                    return false;
//                                }
//                            } else {
//                                alert("Error: Please check that you've entered and confirmed your password!");
//                                form.password.focus();
//                                return false;
//                            }
//
//
//                            return true;
//                        }

    </script>
</html>
