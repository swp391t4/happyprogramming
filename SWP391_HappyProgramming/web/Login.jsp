<%-- 
    Document   : Login
    Created on : May 22, 2022, 4:35:24 PM
    Author     : kingo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Login </title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--===============================================================================================-->	
        <link rel="icon" type="image/png" href="img/images_login/icons/favicon.ico"/>
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/vendor_login/bootstrap/css/bootstrap.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="fonts/fonts_login/font-awesome-4.7.0/css/font-awesome.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="fonts/fonts_login/Linearicons-Free-v1.0.0/icon-font.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/vendor_login/animate/animate.css">
        <!--===============================================================================================-->	
        <link rel="stylesheet" type="text/css" href="vendor/vendor_login/css-hamburgers/hamburgers.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/vendor_login/animsition/css/animsition.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/vendor_login/select2/select2.min.css">
        <!--===============================================================================================-->	
        <link rel="stylesheet" type="text/css" href="vendor/vendor_login/daterangepicker/daterangepicker.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="css/css_login/util.css">
        <link rel="stylesheet" type="text/css" href="css/css_login/main.css">
        <!--===============================================================================================-->
    </head>
    <body>

        <div class="limiter">
            <div class="container-login100">
                <div class="wrap-login100">
                    <div class="login100-form-title" style="background-image: url(https://cdn2.vectorstock.com/i/1000x1000/49/46/coding-and-programming-background-vector-19854946.jpg);">
                        <span class="login100-form-title-1">
                            <h1><a href="home" style="color: white; font-size: 2.5rem">Happy Programming</a></h1>
                            <h3>Đăng nhập</h3>
                        </span>
                    </div>

                    <form class="login100-form validate-form" action="login" method="post">
                        <input type="hidden" name="go" value="login">
                        <p class="text-danger">${mess}</p>
                        <div class="wrap-input100 validate-input m-b-26" data-validate="Username is required">
                            <span class="label-input100">Tài khoản</span>
                            <input class="input100" type="text" name="username" placeholder="Nhập tài khoản" value="${usern}">
                            <span class="focus-input100"></span>
                        </div>
                        <div class="wrap-input100 validate-input m-b-18" data-validate = "Password is required">
                            <span class="label-input100">Mật khẩu</span>
                            <input class="input100" type="password" name="password" placeholder="Nhập mật khẩu" value="${passw}">
                            <span class="focus-input100"></span>
                        </div>

                        <div class="flex-sb-m w-full p-b-30">
                            <div class="contact100-form-checkbox">
                                <input class="input-checkbox100" id="ckb1" type="checkbox" name="remember">
                                <label class="label-checkbox100" for="ckb1">
                                    Nhớ mật khẩu
                                </label>
                            </div>

                            <div>
                                <a href="login?go=changePassword" class="txt1">
                                    Đổi mật khẩu
                                </a>
                            </div>

                            <div>
                                <a href="login?go=signup" class="txt1">
                                    Đăng ký
                                </a>
                            </div>

                            <div>
                                <a href="login?go=resetPassword" class="txt1">
                                    Quên mật khẩu?
                                </a>
                            </div>
                        </div>

                        <div class="container-login100-form-btn">
                            <button style="background-color:  mediumorchid" type="submit" class="login100-form-btn">
                                Đăng nhập
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!--===============================================================================================-->
        <script src="vendor/jquery/jquery-3.2.1.min.js"></script>
        <!--===============================================================================================-->
        <script src="vendor/animsition/js/animsition.min.js"></script>
        <!--===============================================================================================-->
        <script src="vendor/bootstrap/js/popper.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
        <!--===============================================================================================-->
        <script src="vendor/select2/select2.min.js"></script>
        <!--===============================================================================================-->
        <script src="vendor/daterangepicker/moment.min.js"></script>
        <script src="vendor/daterangepicker/daterangepicker.js"></script>
        <!--===============================================================================================-->
        <script src="vendor/countdowntime/countdowntime.js"></script>
        <!--===============================================================================================-->
        <script src="js/main.js"></script>

    </body>
</html>


</html>


