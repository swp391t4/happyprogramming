
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>


<html lang="en">
    <head>
        <title>Login </title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--===============================================================================================-->	
        <link rel="icon" type="image/png" href="image/images_login/icons/favicon.ico"/>
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/vendor_login/bootstrap/css/bootstrap.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="fonts/fonts_login/font-awesome-4.7.0/css/font-awesome.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="fonts/fonts_login/Linearicons-Free-v1.0.0/icon-font.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/vendor_login/animate/animate.css">
        <!--===============================================================================================-->	
        <link rel="stylesheet" type="text/css" href="vendor/vendor_login/css-hamburgers/hamburgers.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/vendor_login/animsition/css/animsition.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/vendor_login/select2/select2.min.css">
        <!--===============================================================================================-->	
        <link rel="stylesheet" type="text/css" href="vendor/vendor_login/daterangepicker/daterangepicker.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="css/css_login/util.css">
        <link rel="stylesheet" type="text/css" href="css/css_login/main.css">
        <!--===============================================================================================-->
    </head>
    <body>

        <div class="limiter">
            <div class="container-login100">
                <div class="wrap-login100">
                    <div class="login100-form-title" style="background-image: url(https://cdn2.vectorstock.com/i/1000x1000/49/46/coding-and-programming-background-vector-19854946.jpg);">
                        <span class="login100-form-title-1">
                            <h1><a href="home" style="color: white; font-size: 2.5rem">Happy Programming</a></h1>
                            <h3>Xác nhận email</h3>
                        </span>
                    </div>

                    <form class="login100-form validate-form" action="login" method="post">
                        <input type="hidden" name="go" value="verifymail">
                        <input type="hidden" name="username" value="${username}">
                        <input type="hidden" name="password" value="${password}">
                        <input type="hidden" name="name" value="${name}">
                        <input type="hidden" name="phone" value="${phone}">
                        <input type="hidden" name="email" value="${email}">
                        <input type="hidden" name="address" value="${address}">
                        <input type="hidden" name="gender" value="${gender}">
                        <input type="hidden" name="role" value="${role}">
                        <input type="hidden" name="dob" value="${dob}">
                        <input type="hidden" name="ma" value="${ma}">
                        <p class="text-danger">${messs}</p>
                        <div class="wrap-input100 validate-input m-b-26" data-validate="Username is required">
                            <span class="label-input100">Mã xác nhận</span>
                            <input class="input100" type="text" name="code" placeholder="Nhập mã xác nhận">
                            <span class="focus-input100"></span>
                        </div>

                        <div class="container-login100-form-btn">
                            <input style="text-align: center" type="submit" id="btn-ChangeP" class="login100-form-btn" name="submit" value="Xác nhận">
                            <!--                                Đổi mật khẩu
                                                        </button>-->
                        </div>

                        
                    </form>
                </div>
            </div>
        </div>

        <!--===============================================================================================-->
        <script src="vendor/jquery/jquery-3.2.1.min.js"></script>
        <!--===============================================================================================-->
        <script src="vendor/animsition/js/animsition.min.js"></script>
        <!--===============================================================================================-->
        <script src="vendor/bootstrap/js/popper.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
        <!--===============================================================================================-->
        <script src="vendor/select2/select2.min.js"></script>
        <!--===============================================================================================-->
        <script src="vendor/daterangepicker/moment.min.js"></script>
        <script src="vendor/daterangepicker/daterangepicker.js"></script>
        <!--===============================================================================================-->
        <script src="vendor/countdowntime/countdowntime.js"></script>
        <!--===============================================================================================-->
        <script src="js/main.js"></script>

    </body>
</html>





