/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.CommentAndRate;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Date;

/**
 *
 * @author kingo
 */
public class CaRDAO {

    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    public int addEvaluation(CommentAndRate CaR) throws Exception {
        int res = 0;
        String sql = "INSERT INTO [dbo].[CommentAndRate]\n"
                + "           ([date]\n"
                + "           ,[from]\n"
                + "           ,[to]\n"
                + "           ,[Content]\n"
                + "           ,[Star])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?)";
        conn = new DBContext().getConnection();
        ps = conn.prepareStatement(sql);
        ps.setTimestamp(1, CaR.getDate());
        ps.setString(2, CaR.getFrom());
        ps.setString(3, CaR.getTo());
        ps.setString(4, CaR.getContent());
        ps.setInt(5, CaR.getStar());
        res = ps.executeUpdate();
        return res;
    }
    
    public static void main(String[] args) throws Exception {
        CaRDAO card = new CaRDAO();
        Date d = new Date();
        
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        System.out.println(card.addEvaluation(new CommentAndRate(timestamp, "username2", "username4", "ok", 4)));
    }
}
