/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.Request;
import Model.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;

/**
 *
 * @author kingo
 */
public class UserDAO {

    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    public static User checkUser(String username) {
        try {
            String sql = "select*from [User] where Username = ? ";
            Connection conn = new DBContext().getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, username);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return new User(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getInt(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getString(10), rs.getString(11), rs.getString(12), rs.getString(13), rs.getInt(14));
            }
        } catch (Exception ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    //Role = 1 là Mentor vì database đang để là kiểu int
    public List<User> getallUserByRole() {
        List<User> list = new ArrayList<>();
        String sql = "select * from [User] where Role = 3 ";
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();

            while (rs.next()) {
                User u = new User(rs.getString("Username"),
                        rs.getString("Password"),
                        rs.getString("Email"),
                        rs.getString("Name"),
                        rs.getInt("Gender"),
                        rs.getString("PhoneNumber"),
                        rs.getString("Address"),
                        rs.getString("DOB"),
                        rs.getString("Img"),
                        rs.getString("serviceDesc"),
                        rs.getString("archivementDesc"),
                        rs.getString("profession"),
                        rs.getString("professionIntro"),
                        rs.getInt("Role"));
                list.add(u);
            }
        } catch (Exception e) {

        }
        return list;

    }

    public User getUserByUsername(String username) {
        String sql = "select * from [User] where username = ?";
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, username);
            rs = ps.executeQuery();
            while (rs.next()) {
                return new User(rs.getString("Username"),
                        rs.getString("Password"),
                        rs.getString("Email"),
                        rs.getString("Name"),
                        rs.getInt("Gender"),
                        rs.getString("PhoneNumber"),
                        rs.getString("Address"),
                        rs.getString("DOB"),
                        rs.getString("Img"),
                        rs.getString("serviceDesc"),
                        rs.getString("archivementDesc"),
                        rs.getString("profession"),
                        rs.getString("professionIntro"),
                        rs.getInt("Role"));
            }
        } catch (Exception e) {
        }
        return null;
    }

    public int getTotal(int Role) {
        String sql = "select count(*) from [User] where Role = ?";
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setInt(1, Role);
            rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return 0;
    }

    public int getTotalMentorByName(int Role, String name) {
        String sql = "select count(*) from [User] where Role = ? and Name like ?";
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setInt(1, Role);
            ps.setString(2, "%"+name);
            rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return 0;
    }

    public List<User> pagingUser(int index) {
        List<User> list = new ArrayList<>();
        String sql = "select * from [User] where Role = 3\n"
                + "order by Name\n"
                + "offset ? row fetch next 3 row only";

        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setInt(1, (index - 1) * 3);
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new User(rs.getString("Username"),
                        rs.getString("Password"),
                        rs.getString("Email"),
                        rs.getString("Name"),
                        rs.getInt("Gender"),
                        rs.getString("PhoneNumber"),
                        rs.getString("Address"),
                        rs.getString("DOB"),
                        rs.getString("Img"),
                        rs.getString("serviceDesc"),
                        rs.getString("archivementDesc"),
                        rs.getString("profession"),
                        rs.getString("professionIntro"),
                        rs.getInt("Role")));
            }
        } catch (Exception e) {
        }
        return list;
    }

    public User getUserByEmail(String username, String email) throws SQLException, Exception {
        User user = new User();
        String sql = "Select username, email from [User] where username = ? and email = ?";
        conn = new DBContext().getConnection();
        ps = conn.prepareStatement(sql);
        ps.setString(1, username);
        ps.setString(2, email);
        rs = ps.executeQuery();
        if (rs.next()) {
            user.setUsername(rs.getString(1));
            user.setEmail(rs.getString(2));
        }
        return user;
    }

    public User login(String username, String password) throws Exception {
        User user = new User();
        String sql = "Select * from [User] where username = ? and password = ?";
        conn = new DBContext().getConnection();
        ps = conn.prepareStatement(sql);
        ps.setString(1, username);
        ps.setString(2, password);
        rs = ps.executeQuery();
        if (rs.next()) {
            user.setUsername(rs.getString(1));
            user.setPassword(rs.getString(2));
            user.setEmail(rs.getString(3));
            user.setName(rs.getString(4));
            user.setGender(rs.getInt(5));
            user.setPhone(rs.getString(6));
            user.setAddress(rs.getString(7));
            user.setDOB(rs.getString(8));
            user.setImg(rs.getString(9));
            user.setServiceDesc(rs.getString(10));
            user.setArchivementDesc(rs.getString(11));
            user.setProfession(rs.getString(12));
            user.setProfessionIntro(rs.getString(13));
            user.setRole(rs.getInt(14));
        }
        return user;
    }

    public int changePassword(String username, String newPass) throws Exception {
        String sql = "update [User] set [Password] = ? where username = ?";
        conn = new DBContext().getConnection();
        ps = conn.prepareStatement(sql);
        ps.setString(1, newPass);
        ps.setString(2, username);
        int res = ps.executeUpdate();

        return res;
    }

    public int getTotalMentor(String sid) throws Exception {
        ArrayList<User> lm = new ArrayList<>();
        String sql = "select COUNT(distinct u.Username) from [user] u\n"
                + "inner join Mentor_Skill ms\n"
                + "on u.Username = ms.username\n"
                + "where [role] = 3 and [sid] in (" + sid + ")";
        conn = new DBContext().getConnection();
        ps = conn.prepareStatement(sql);
        rs = ps.executeQuery();
        if (rs.next()) {
            return rs.getInt(1);
        }
        return 0;
    }

    public float getAvgRateByUsername(String username) throws Exception {
        float res = 0;
        String sql = "select AVG(CAST(Star AS FLOAT)) from CommentAndRate where [to]  = ?";
        conn = new DBContext().getConnection();
        ps = conn.prepareStatement(sql);
        ps.setString(1, username);
        rs = ps.executeQuery();
        if (rs.next()) {
            res = rs.getFloat(1);
        }
        return res;
    }

    public int getNumOfReqReceivedByUser(String username) throws Exception {
        int res = 0;
        String sql = "select COUNT([to]) from Request where [to] = ?";
        conn = new DBContext().getConnection();
        ps = conn.prepareStatement(sql);
        ps.setString(1, username);
        rs = ps.executeQuery();
        if (rs.next()) {
            res = rs.getInt(1);
        }
        return res;
    }

    public int getNumOfReqInvitedByUser(String username) throws Exception {
        int res = 0;
        String sql = "select COUNT([from]) from Request where [from] = ?";
        conn = new DBContext().getConnection();
        ps = conn.prepareStatement(sql);
        ps.setString(1, username);
        rs = ps.executeQuery();
        if (rs.next()) {
            res = rs.getInt(1);
        }
        return res;
    }

    public ArrayList<User> pagingMentor(int index, String sid) throws Exception {
        ArrayList<User> lu = new ArrayList<>();
        String sql = "select [name], u.Username, img, (select FORMAT(AVG(CAST(Star AS FLOAT)),'N1') from CommentAndRate where [to]  = u.Username) as rate from [user] u\n"
                + "inner join Mentor_Skill ms\n"
                + "on u.Username = ms.username\n"
                + "where [role] = 3 and [sid] in (" + sid + ")\n"
                + "group by [name], u.Username, img\n"
                + "order by rate desc\n"
                + "OFFSET ? ROWS FETCH NEXT 2 ROWS ONLY";
        conn = new DBContext().getConnection();
        ps = conn.prepareStatement(sql);
        ps.setInt(1, (index - 1) * 2);
        rs = ps.executeQuery();
        while (rs.next()) {
            User u = new User();
            u.setName(rs.getString(1));
            u.setUsername(rs.getString(2));
            u.setImg(rs.getString(3));
            lu.add(u);
        }
        return lu;
    }

    public ArrayList<User> getListMentor() throws Exception {
        ArrayList<User> lm = new ArrayList<>();
        String sql = "select [Name], Username from [User] u\n"
                + "inner join Request r\n"
                + "on r.[to] = u.Username\n"
                + "where role = 3\n"
                + "group by r.[to],[Name],Username";
        conn = new DBContext().getConnection();
        ps = conn.prepareStatement(sql);
        rs = ps.executeQuery();
        while (rs.next()) {
            User u = new User();
            u.setName(rs.getString(1));
            u.setUsername(rs.getString(2));
            lm.add(u);
        }
        return lm;
    }

//    public static void main(String[] args) throws Exception {        
//        UserDAO us = new UserDAO();
//        ArrayList<Integer> numOfReq = new ArrayList<Integer>();
//        ArrayList<Float> listRate = new ArrayList<>();
//        ArrayList<User> list = us.pagingMentor(5, 1);
//        for (User u : list) {
//            int req = us.getNumOfReqReceivedByUser(u.getUsername());
//            float rate = us.getAvgRateByUsername(u.getUsername());
//            System.out.println(u.getName() + "\t" + u.getUsername() + "\t" + req + "\t" + rate);
//        }
//
//    }
//
// 
    public static void signUp(String username, String password, String email, String name, int gender, String phone, String address, String DOB, String img, int role) {
        try {
            String sql = "insert into [User](Username,[Password],Email,[Name],Gender,PhoneNumber,[Address],DOB,img,[Role]) values\n"
                    + "(?,?,?,?,?,?,?,?,?,?)";
            Connection conn = new DBContext().getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, username);
            ps.setString(2, password);
            ps.setString(3, email);
            ps.setString(4, name);
            ps.setInt(5, gender);
            ps.setString(6, phone);
            ps.setString(7, address);
            ps.setString(8, DOB);
            ps.setString(9, img);
            ps.setInt(10, role);
            ps.executeUpdate();

        } catch (Exception ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public User getallStar(String username, int star) {

        String sql = "select count(Star) as star from CommentAndRate where [to] = ? and Star = ?";
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, username);
            ps.setInt(2, star);
            rs = ps.executeQuery();
            while (rs.next()) {
                return new User(rs.getInt("Star")
                );

            }
        } catch (Exception e) {
        }
        return null;
    }

    public void insertCvMentor(String servicedesc, String archivementDesc, String profession, String professionIntro) {
        String sql = "insert into [User](serviceDesc,archivementDesc,profession,professionIntro) values \n"
                + "(?,?,?,?)";
        try {
            ps = conn.prepareStatement(sql);
            ps.setString(1, servicedesc);
            ps.setString(2, archivementDesc);
            ps.setString(3, profession);
            ps.setString(4, professionIntro);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    public void UpdateCvMentor(String name,int gender, String phone, String add, String dob, String img, String service, String arch, String profession, String proIntro, String username) {
        String sql = "UPDATE [dbo].[User]\n"
                + "   SET\n"
                + "      [Name] = ?\n"
                + "      ,[Gender] = ?\n"
                + "      ,[PhoneNumber] =?\n"
                + "      ,[Address] = ?\n"
                + "      ,[DOB] = ?\n"
                + "      ,[Img] = ?\n"
                + "      ,[serviceDesc] = ?\n"
                + "      ,[archivementDesc] = ?\n"
                + "      ,[profession] = ?\n"
                + "      ,[professionIntro] =?\n"
                + " WHERE Username =?";
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, name);
            ps.setInt(2, gender);
            ps.setString(3, phone);
            ps.setString(4, add);
            ps.setString(5, dob);
            ps.setString(6, img);
            ps.setString(7, service);
            ps.setString(8, arch);
            ps.setString(9, profession);
            ps.setString(10, proIntro);
            ps.setString(11, username);
            ps.executeUpdate();
        } catch (Exception ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void UpdateProfile(String name,int gender, String phone, String add, String dob, String img, String username) {
        String sql = "UPDATE [dbo].[User]\n"
                + "   SET\n"
                + "      [Name] = ?\n"
                + "      ,[Gender] = ?\n"
                + "      ,[PhoneNumber] =?\n"
                + "      ,[Address] = ?\n"
                + "      ,[DOB] = ?\n"
                + "      ,[Img] = ?\n"
                + " WHERE Username =?";
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, name);
            ps.setInt(2, gender);
            ps.setString(3, phone);
            ps.setString(4, add);
            ps.setString(5, dob);
            ps.setString(6, img);
            ps.setString(7, username);
            ps.executeUpdate();
        } catch (Exception ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public int getTotalMentee() throws Exception {
        String sql = "select count(*) from [User] where role = 2";
        conn = new DBContext().getConnection();
        ps = conn.prepareStatement(sql);
        rs = ps.executeQuery();
        if (rs.next()) {
            return rs.getInt(1);
        }
        return 0;
    }

    public int getTotalHoursOfAllReq() throws Exception {
        String sql = "select SUM(deadlineHour) as 'TotalHour' from [user] u\n"
                + "inner join Request r \n"
                + "on u.Username = r.[from]\n"
                + "where role = 2 and r.status = 2\n"
                + "group by  u.Role, r.status\n"
                + "select * from Request";
        conn = new DBContext().getConnection();
        ps = conn.prepareStatement(sql);
        rs = ps.executeQuery();
        if (rs.next()) {
            return rs.getInt(1);
        }
        return 0;
    }

    public int getTotalOfSkillOfAllReq() throws Exception {
        String sql = "select  COUNT(distinct [sid]) from Request r\n"
                + "inner join [User] u \n"
                + "on r.[from] = u.Username\n"
                + "inner join RequestDetail rd \n"
                + "on rd.rid = r.id\n"
                + "where u.[Role] = 2 and r.[status] = 2";
        conn = new DBContext().getConnection();
        ps = conn.prepareStatement(sql);
        rs = ps.executeQuery();
        if (rs.next()) {
            return rs.getInt(1);
        }
        return 0;
    }

    public ArrayList<User> pagingMentee(int index) throws Exception {
        ArrayList<User> lu = new ArrayList<>();
        String sql = "select * from [User] where role = 2\n"
                + "order by [Name]\n"
                + "OFFSET ? ROWS FETCH NEXT 2 ROWS ONLY";
        conn = new DBContext().getConnection();
        ps = conn.prepareStatement(sql);
        ps.setInt(1, (index-1)*2);
        rs = ps.executeQuery();
        while(rs.next()){
            User u  = new User();
            u.setUsername(rs.getString("Username"));
            u.setName(rs.getString("Name"));
            lu.add(u);
        }
        return lu;
    }

    public List<User> getallUserBySearch(String name, int index) {
        List<User> list = new ArrayList<>();
        String sql = "select * from [User] where Role = 3 and Name like ?\n"
                + "order by Name\n"
                + "offset ? row fetch next 3 row only";

        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1,"%" + name);
            ps.setInt(2, (index - 1) * 3);
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new User(rs.getString("Username"),
                        rs.getString("Password"),
                        rs.getString("Email"),
                        rs.getString("Name"),
                        rs.getInt("Gender"),
                        rs.getString("PhoneNumber"),
                        rs.getString("Address"),
                        rs.getString("DOB"),
                        rs.getString("Img"),
                        rs.getString("serviceDesc"),
                        rs.getString("archivementDesc"),
                        rs.getString("profession"),
                        rs.getString("professionIntro"),
                        rs.getInt("Role")));
            }
        } catch (Exception e) {
        }
        return list;

    }

    public void UpdateStar(int star, String username) {
        String sql = "update CommentAndRate set Star = ? where [from] = Admin and [to] = ? ";
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setInt(1, star);
            ps.setString(2, username);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    public static void main(String[] args) throws Exception {


        UserDAO dao = new UserDAO();
        List<User> listPage = dao.pagingUser(1);
        RequestDAO reqdao = new RequestDAO();
        List<User> list = dao.getallUserBySearch("Mạnh", 1);
      int count = dao.getTotalMentorByName(3, "Tú");
        System.out.println(count);
        
//        int numaccept = 0;
//        int numcancel = 0;
//        int numcomplete = 0;
//        double percentofcomplete = 0;
//        for (int i = 0; i < listPage.size(); i++) {
//            List<Request> req = reqdao.getReqInviting(listPage.get(i).getUsername());
//            for (int y = 0; y < req.size(); y++) {
//
//                if (req.get(y).getStatus() == 2) {
//
//                    numaccept++;
//                }
//                if (req.get(y).getStatus() == 4) {
//                    numcancel++;
//                }
//                if (req.get(y).getStatus() == 3) {
//                    numcomplete++;
//                }
//
//            }
//
//            String num1 = String.valueOf(numaccept);
////                         accept.add(num1);
//            numaccept = 0;
//            String num2 = String.valueOf(numcancel);
////                        cancel.add(num2);
//            numcancel = 0;
//            double percentofcompleted = 0;
//            System.out.println(numcomplete);
//            if (req.size() != 0) {
//                percentofcompleted = (2 / 5) * 100;
//                System.out.println(percentofcompleted);
//                numcomplete = 0;
//
//            }

//                        complete.add(num3);
//            numcomplete = 0;
//                        session.setAttribute("accept", accept);
//                        session.setAttribute("cancel", cancel);
//                        session.setAttribute("complete", complete);
//        }

//        System.out.println(a);
//          System.out.println(us.getNumOfReqInvitedByUser("Hoanghai"));
//        ArrayList<Integer> numOfReq = new ArrayList<Integer>();
//        ArrayList<Float> listRate = new ArrayList<>();
//        System.out.println(us.getTotalMentor(2));
//        ArrayList<User> l = us.pagingMentor(1, 1);
//        double a = 6.550;
//        System.out.println(String.format("%.1f", a));
//        for (User u : l) {
//            int req = us.getNumOfReqReceivedByUser(u.getUsername());
//            float rate = us.getAvgRateByUsername(u.getUsername());
//            int reqi = us.getNumOfReqInvitedByUser(u.getUsername());
//            System.out.println(u.getName() + "\t" + u.getUsername() + "\t" + req + "\t" + String.format("%.1f", rate) + "\t" + reqi);
//        }
    }

}
