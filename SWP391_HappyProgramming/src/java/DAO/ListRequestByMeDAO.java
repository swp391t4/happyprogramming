/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.Request1;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author laptop 2019
 */
public class ListRequestByMeDAO {

    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    public ArrayList<Request1> ListRequest(String user) {
        ArrayList<Request1> la = new ArrayList<>();
        String sql = "select r.id  , r.[from], r.[to], r.Title, r.DateReq,r.deadlineDate,r.deadlineHour, r.status from RequestDetail rd join Skill s\n"
                + "on rd.[sid] = s.id join Request r\n"
                + "on r.id = rd.rid\n"
                + "where [from] = ? ";
        try {

            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, user);
            rs = ps.executeQuery();
            while (rs.next()) {
                la.add(new Request1(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getDate(5),
                        rs.getDate(6),
                        rs.getInt(7),
                        rs.getInt(8)));
            }
        } catch (Exception e) {
        }
        return la;
    }

    public void deleteRequest(String sid) {
        String sql = "delete  rd from RequestDetail rd join Skill s\n"
                + "on rd.[sid] = s.id join Request r\n"
                + "on r.id = rd.rid\n"
                + "where r.id = ?";

        try {

            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, sid);
            ps.executeUpdate();

        } catch (Exception e) {
        }
    }


}
