/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

/**
 *
 * @author kingo
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class DBContext {
    
    /*USE BELOW METHOD FOR YOUR DATABASE CONNECTION FOR BOTH SINGLE AND MULTILPE SQL SERVER INSTANCE(s)*/
   private static final String serverName = "DESKTOP-LAFLJQD";
    private static final String dbName = "HappyProgramming";
    private static final String portNumber = "1433";
    private static final String user = "sa";
    private static final String password = "123456";
    
    public static Connection getConnection() throws Exception {
        
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

            String url = "jdbc:sqlserver://" + serverName + ":" + portNumber + ";databaseName=" + dbName;
            Connection conn = DriverManager.getConnection(url, user, password);
            System.out.println("Kết nối thành công tới Database: "+ conn.getCatalog());
            
            return conn;
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
        
}
  
}
 