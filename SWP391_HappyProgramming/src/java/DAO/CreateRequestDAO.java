/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author laptop 2019
 */
public class CreateRequestDAO {

    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    public int createRequest(String user, String title, Date da, 
            Date deadlineDate, String deadlineHour, int sta) {
        
        String sql = "INSERT INTO [HappyProgramming].[dbo].[Request]\n"
                + "           ([from]\n"
               
                + "           ,[Title]\n"
                + "           ,[DateReq]\n"
                + "           ,[deadlineDate]\n"
                + "           ,[deadlineHour]\n"
                + "           ,[status])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?)";
int a=0;

        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, user);
            ps.setString(2, title);
            ps.setDate(3, da);
            ps.setDate(4, deadlineDate);

            ps.setString(5, deadlineHour);
            ps.setInt(6, sta);

           a= ps.executeUpdate();
return a;
        } catch (Exception e) {
        }
        return a;
    }

    public void createRequestDetail(int rid, String c1) {
        String sql = "insert into RequestDetail values(?, ?)";

        try {

            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setInt(1, rid);
            ps.setString(2, c1);
            ps.executeUpdate();

        } catch (Exception e) {
        }
    }

     public int getLastRequest() {
        String sql = "select top 1 id  from Request \n"
                + "order by id desc";
        int a =0;
        try {

            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            while(rs.next()){
             a = rs.getInt(1) ;
            }
        } catch (Exception e) {
        }
         return a;
    }

    public static void main(String[] args) {
//        CreateRequestDAO db = new CreateRequestDAO();
//       int a = db.createRequest("abcd", "asdasd", 2022-12-23, "2022-10-23", "3", 1);
//        System.out.println(a);

    }
}
