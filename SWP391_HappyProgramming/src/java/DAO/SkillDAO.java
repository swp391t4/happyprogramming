/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.Skill;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author USER
 */
public class SkillDAO {

    public List<Skill> getAllSkills() {
        List<Skill> list = new ArrayList<>();
        try {
            String sql = "select * from Skill";
            Connection conn = new DBContext().getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Skill p = new Skill(rs.getInt(1), rs.getString(2), rs.getInt(3));
                list.add(p);
            }
        } catch (Exception ex) {
            Logger.getLogger(SkillDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public static void addNewSkill(String name, int status) {
        String sql = "insert into Skill (Name,status) values(?,?)";
        try {
            Connection conn = new DBContext().getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, name);
            ps.setInt(2, status);
            ps.executeUpdate();
        } catch (Exception ex) {
            Logger.getLogger(SkillDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static Skill checkSKillName(String skillname) {
        try {
            String sql = "select*from Skill where Name = ? ";
            Connection conn = new DBContext().getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, skillname);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return new Skill(rs.getInt(1), rs.getString(2), rs.getInt(3));
            }
        } catch (Exception ex) {
            Logger.getLogger(SkillDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public int getTotalSkill() throws Exception {

        String sql = "select COUNT(*) from Skill ";
        Connection conn = new DBContext().getConnection();
        PreparedStatement ps = conn.prepareStatement(sql);
        ResultSet rs = ps.executeQuery();
        if (rs.next()) {
            return rs.getInt(1);
        }
        return 0;
    }

    public ArrayList<Skill> pagingSkill(int index, int role) throws Exception {
        ArrayList<Skill> lu = new ArrayList<>();
        String sql = "";
        if (role == 4) {
             sql = "select*from Skill order by [Name] offset ? ROWS FETCH NEXT 2 ROWS ONLY";
        } else {
             sql = "select*from Skill where status = 1 order by [Name] offset ? ROWS FETCH NEXT 2 ROWS ONLY";
        }
        Connection conn = new DBContext().getConnection();
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setInt(1, (index - 1) * 2);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            Skill u = new Skill();
            u.setId(rs.getInt(1));
            u.setName(rs.getString(2));
            u.setStatus(rs.getInt(3));
            lu.add(u);
        }
        return lu;
    }


    public static void updateSkill(Skill sk) {
        try {
            String sql = "UPDATE [dbo].[Skill]\n"
                    + "   SET [Name] = ?\n"
                    + "      ,[status] = ?\n"
                    + " WHERE id = ?";
            Connection conn = new DBContext().getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, sk.getName());
            ps.setInt(2, sk.getStatus());
            ps.setInt(3, sk.getId());
            ps.executeUpdate();
        } catch (Exception ex) {
            Logger.getLogger(SkillDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Skill getSkillById(int id) {
        String sql = "select*from Skill where id = ?";
        Connection con;
        try {
            con = new DBContext().getConnection();
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                   Skill sk = new Skill(rs.getInt(1), rs.getString(2), rs.getInt(3));
                            return sk;
    }
                    }catch(Exception ex){
                    
                    }
        return null;
    }

    public List<Skill> getAllSkillsMentor(String username) {
        List<Skill> list = new ArrayList<>();
        try {
            String sql = "select * from Mentor_Skill where username =?";
            Connection conn = new DBContext().getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, username);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Skill p = new Skill(rs.getInt(2),null, 0);
                list.add(p);
            }
        } catch (Exception ex) {
            Logger.getLogger(SkillDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return list;
    }

    public static void delete(int id) {
        try {
            String sql = "DELETE FROM [dbo].[Skill]\n"
                    + "      WHERE id=?";
            Connection conn = new DBContext().getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (Exception ex) {
            Logger.getLogger(SkillDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    
    
    
    
    public static void main(String[] args) throws Exception {
        SkillDAO skd = new SkillDAO();
        Skill sk = new Skill();
        List<Skill> list = new ArrayList<>();

        System.out.println(skd.getAllSkills());
//     if(skd.checkSKillName("git")==null){
//         System.out.println("ko trùng");
//     }else{
//         System.out.println("trùng");
//     }



     List<Skill> ls = skd.getAllSkillsMentor("Hoanghai");
        for (Skill l : ls) {
            System.out.println(l);
        }
     

    }

}
