package DAO;

import Model.Request;
import Model.Request1;
import Model.Skill;

import Model.User;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import static java.util.Collections.list;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author USER
 * @author Admin
 */
public class RequestDAO {

    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    public List<Request> getReqInviting(String username) {
        ArrayList<Request> list = new ArrayList<>();
        String sql = "select * from Request where [to] = ? ";
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, username);
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Request(
                        rs.getInt("id"),
                        rs.getString("from"),
                        rs.getString("to"),
                        rs.getString("Title"),
                        rs.getString("DateReq"),
                        rs.getString("deadlineDate"),
                        rs.getInt("deadlineHour"),
                        rs.getInt("status")
                ));
            }
        } catch (Exception e) {

        }
        return list;
    }

    public void UpdateStatus(int status, int id) {
        String sql = "update Request set Request.status = ? where Request.id = ?";
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setInt(1, status);
            ps.setInt(2, id);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    public List<Skill> getlistReqInviting(String username) {
        ArrayList<Skill> lists = new ArrayList<>();
        String sql = "select * from Skill s\n"
                + "inner join RequestDetail rd on rd.sid = s.id  \n"
                + "inner join Request r on r.id = rd.rid where r.[to]=?";

        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, username);
            rs = ps.executeQuery();
            while (rs.next()) {
                lists.add(new Skill(
                        rs.getInt("id"),
                        rs.getString("Name"),
                        rs.getInt("status")));
            }
        } catch (Exception e) {

        }
        return lists;
    }

    public void insertSkill(String name) throws Exception {
        String sql = "insert into Skill([Name]) value\n"
                + "(?)";
        conn = new DBContext().getConnection();
        ps = conn.prepareStatement(sql);
        ps.setString(1, name);
        ps.executeUpdate();
    }

    public void insertSkillMentor(String name, int sid) throws Exception {
        String sql = "insert into Mentor_Skill values\n"
                + "(?,?)";
        conn = new DBContext().getConnection();
        ps = conn.prepareStatement(sql);
        ps.setString(1, name);
        ps.setInt(2, sid);
        ps.executeUpdate();
    }

    public ArrayList<Skill> getSkill() {
        ArrayList<Skill> list = new ArrayList<>();
        String sql = "select * from Skill";
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Skill(
                        rs.getInt("id"),
                        rs.getString("Name"),
                        rs.getInt("status")
                ));
            }
        } catch (Exception e) {

        }
        return list;
    }

    public int getTotal(String username) {
        String sql = "select count(*) from Request where [to] = ?";
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, username);
            rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {

        }
        return 0;
    }

    public List<Request> pagingRequest(String username, int index) {
        List<Request> list = new ArrayList<>();
        String sql = "select [from],Title,Request.status,Skill.Name as Skill \n"
                + "from Request join RequestDetail on Request.id = RequestDetail.rid \n"
                + "join Skill on RequestDetail.sid  = Skill.id  \n"
                + "where [to] = ?\n"
                + "order by [from] \n"
                + "offset ? row fetch next 4 row only";

        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, username);
            ps.setInt(2, (index - 1) * 4);
            rs = ps.executeQuery();
            while (rs.next()) {
//                Request r = new Request();
//                r.setFrom(rs.getString("from"));
//                r.setTitle(rs.getString("Title"));
//                r.setStatus(rs.getInt("status"));
//                r.set(rs.getString("from"));
                list.add(new Request(rs.getString("from"),
                        rs.getString("Title"),
                        rs.getInt("status"),
                        rs.getString("Skill")));
            }
        } catch (Exception e) {

        }
        return list;
    }

    public List<Request> getallStatusRequest(String username) {
        List<Request> list = new ArrayList<>();
        String sql = "select [from],Title,Request.status,Skill.Name as Skill \n"
                + "from Request join RequestDetail on Request.id = RequestDetail.rid \n"
                + "join Skill on RequestDetail.sid  = Skill.id  \n"
                + "where [to] = ?";
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, username);
            rs = ps.executeQuery();
            while (rs.next()) {
                Request req = new Request(rs.getString("from"),
                        rs.getString("Title"),
                        rs.getInt("status"),
                        rs.getString("Skill"));
                list.add(req);
            }
        } catch (Exception e) {
        }
        return list;
    }

    public ArrayList<Skill> getSkillbyMentor(int id) throws Exception {
        ArrayList<Skill> list = new ArrayList<>();
        String sql = "select s.[Name] from Skill s \n"
                + "inner join RequestDetail r on s.id = r.sid  where r.rid =?";
        conn = new DBContext().getConnection();
        ps = conn.prepareStatement(sql);
        ps.setInt(1, id);
        rs = ps.executeQuery();
        while (rs.next()) {
            Skill s = new Skill();
            s.setName(rs.getString("Name"));
            list.add(s);
        }
        return list;
    }

    public int getTotalReq(String name) {
        String sql = "select distinct count(*) from Request  where [to] =?";
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, name);
            rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {

        }
        return 0;
    }

    public int getTotalMentorForMentee() throws Exception {
        String sql = "select count(distinct[to])\n"
                + "from Request";
        Connection conn = new DBContext().getConnection();
        PreparedStatement ps = conn.prepareStatement(sql);
        ResultSet rs = ps.executeQuery();
        if (rs.next()) {
            return rs.getInt(1);
        }
        return 0;
    }

    public int getTotalReq() throws Exception {
        String sql = "select COUNT(*) from Request ";
        Connection conn = new DBContext().getConnection();
        PreparedStatement ps = conn.prepareStatement(sql);
        ResultSet rs = ps.executeQuery();
        if (rs.next()) {
            return rs.getInt(1);

        }
        return 0;
    }

//    public List<Request> pagingRequest(String username, int index) {
//        List<Request> list = new ArrayList<>();
//        String sql = "select [from],Title,Request.status,Skill.Name as Skill \n"
//                + "from Request join RequestDetail on Request.id = RequestDetail.rid \n"
//                + "join Skill on RequestDetail.sid  = Skill.id  \n"
//                + "where [to] = ?\n"
//                + "order by [from] \n"
//                + "offset ? row fetch next 4 row only";
//
//        try {
//            conn = new DBContext().getConnection();
//            ps = conn.prepareStatement(sql);
//            ps.setString(1, username);
//            ps.setInt(2, (index - 1) * 4);
//            rs = ps.executeQuery();
//            while (rs.next()) {
//                list.add(new Request(rs.getString("from"),
//                        rs.getString("Title"),
//                        rs.getInt("status"),
//                        rs.getString("Skill")));
//            }
//        } catch (Exception e) {
//    }
    public List<Request> getAllReq() {
        List<Request> list = new ArrayList<>();
        try {
            String sql = "select * from Request";
            Connection conn = new DBContext().getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Request r = new Request();
                r.setId(rs.getInt(1));
                r.setFrom(rs.getString(2));
                r.setTo(rs.getString(3));
                r.setTitle(rs.getString(4));
                r.setDatereq(rs.getString(5));
                r.setDeadlineDate(rs.getString(6));
                r.setDeadlineHour(rs.getInt(7));
                r.setStatus(rs.getInt(8));
                list.add(r);
            }
        } catch (Exception ex) {
            Logger.getLogger(RequestDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return list;
    }

    public ArrayList<Request> pageRequest(String user, int page) throws Exception {
        ArrayList<Request> list = new ArrayList<>();
        String sql = "select * from Request where [to]=?\n"
                + "order by [from]\n"
                + "offset ? row fetch next 3 row only";
        conn = new DBContext().getConnection();
        ps = conn.prepareStatement(sql);
        ps.setString(1, user);
        ps.setInt(2, (page - 1) * 3);
        rs = ps.executeQuery();
        while (rs.next()) {
            list.add(new Request(
                    rs.getInt("id"),
                    rs.getString("from"),
                    rs.getString("to"),
                    rs.getString("Title"),
                    rs.getString("DateReq"),
                    rs.getString("deadlineDate"),
                    rs.getInt("deadlineHour"),
                    rs.getInt("status")
            ));
        }
        return list;
    }

    public Request getRequestByID(String rid) throws SQLException, Exception {
        Request r = new Request();
        
            String sql = "select * from Request where id = ?";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, rid);
            rs = ps.executeQuery();
            while (rs.next()) {
                r.setId(rs.getInt(1));
                r.setFrom(rs.getString(2));
                r.setTo(rs.getString(3));
                r.setTitle(rs.getString(4));
                r.setDatereq(rs.getString(5));
                r.setDeadlineDate(rs.getString(6));
                r.setDeadlineHour(rs.getInt(7));
                r.setStatus(rs.getInt(8));
            }
            return r;
    }
    
        
    

    public int getTotalHourReq() throws Exception {
        String sql = "select sum(deadlineHour)\n"
                + "from Request ";
        Connection conn = new DBContext().getConnection();
        PreparedStatement ps = conn.prepareStatement(sql);
        ResultSet rs = ps.executeQuery();
        if (rs.next()) {
            return rs.getInt(1);
        }
        return 0;
    }

//    public List<Request> getAllReq() {
//        List<Request> list = new ArrayList<>();
//        try {
//public class RequestDAO {
//    public int getTotalMentorForMentee() throws Exception {
//        String sql = "select count(distinct[to])\n"
//                + "from Request";
//        Connection conn = new DBContext().getConnection();
//        PreparedStatement ps = conn.prepareStatement(sql);
//        ResultSet rs = ps.executeQuery();
//        if (rs.next()) {
//            return rs.getInt(1);
//        }
//        return 0;
//    }
//
//    public int getTotalReq() throws Exception {
//        String sql = "select COUNT(*) from Request ";
//        Connection conn = new DBContext().getConnection();
//        PreparedStatement ps = conn.prepareStatement(sql);
//        ResultSet rs = ps.executeQuery();
//        if (rs.next()) {
//            return rs.getInt(1);
//
//        }
//        return 0;
//    }
//
//
//    public List<Request> pagingRequest(String username, int index) {
//        List<Request> list = new ArrayList<>();
//        String sql = "select [from],Title,Request.status,Skill.Name as Skill \n"
//                + "from Request join RequestDetail on Request.id = RequestDetail.rid \n"
//                + "join Skill on RequestDetail.sid  = Skill.id  \n"
//                + "where [to] = ?\n"
//                + "order by [from] \n"
//                + "offset ? row fetch next 4 row only";
//
//        try {
//            conn = new DBContext().getConnection();
//            ps = conn.prepareStatement(sql);
//            ps.setString(1, username);
//            ps.setInt(2, (index - 1) * 4);
//            rs = ps.executeQuery();
//            while (rs.next()) {
//                list.add(new Request(rs.getString("from"),
//                        rs.getString("Title"),
//                        rs.getInt("status"),
//                        rs.getString("Skill")));
//            }
//        } catch (Exception e) {
//
//    public int getTotalHourReq() throws Exception {
//        String sql = "select sum(deadlineHour)\n"
//                + "from Request ";
//        Connection conn = new DBContext().getConnection();
//        PreparedStatement ps = conn.prepareStatement(sql);
//        ResultSet rs = ps.executeQuery();
//        if (rs.next()) {
//            return rs.getInt(1);
//        }
//        return 0;
//    }
//
//    public List<Request> getAllReq() {
//        List<Request> list = new ArrayList<>();
//        try {
//            String sql = "select * from Request";
//            Connection conn = new DBContext().getConnection();
//            PreparedStatement ps = conn.prepareStatement(sql);
//            ResultSet rs = ps.executeQuery();
//            while (rs.next()) {
//                Request r = new Request();
//                r.setId(rs.getInt(1));
//                r.setFrom(rs.getString(2));
//                r.setTo(rs.getString(3));
//                r.setTitle(rs.getString(4));
//                r.setDatereq(rs.getString(5));
//                r.setDeadlineDate(rs.getString(6));
//                r.setDeadlineHour(rs.getInt(7));
//                r.setStatus(rs.getInt(8));
//                list.add(r);
//            }
//        } catch (Exception ex) {
//            Logger.getLogger(RequestDAO.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        return list;
//    }
//
//    public ArrayList<Request> pageRequest(String user, int page) throws Exception {
//        ArrayList<Request> list = new ArrayList<>();
//        String sql = "select * from Request where [to]=?\n"
//                + "order by [from]\n"
//                + "offset ? row fetch next 3 row only";
//        conn = new DBContext().getConnection();
//        ps = conn.prepareStatement(sql);
//        ps.setString(1, user);
//        ps.setInt(2, (page - 1) * 3);
//        rs = ps.executeQuery();
//        while (rs.next()) {
//            list.add(new Request(
//                    rs.getInt("id"),
//                    rs.getString("from"),
//                    rs.getString("to"),
//                    rs.getString("Title"),
//                    rs.getString("DateReq"),
//                    rs.getString("deadlineDate"),
//                    rs.getInt("deadlineHour"),
//                    rs.getInt("status")
//            ));
//        }
//        return list;
//    }
//
//   
//
//    public int getTotalHourReq() throws Exception {
//        String sql = "select sum(deadlineHour)\n"
//                + "from Request ";
// public class RequestDAO {
//            System.out.println(r);
//
//        }
//
    public Request1 getDetailRequest(String id) {

        try {
            String sql = "select * from Request\n"
                    + "  where id = ?";
            Connection conn = new DBContext().getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Request1 r = new Request1(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getDate(5), rs.getDate(6),
                        rs.getInt(7), rs.getInt(8)
                );
                return r;
            }
        } catch (Exception ex) {
            Logger.getLogger(RequestDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public List<Skill> getSkill_Req(String rid) {
        List<Skill> list = new ArrayList<>();
        try {
            String sql = "select s.id, s.[Name], s.[status] from RequestDetail rd\n"
                    + "inner join Skill s \n"
                    + "on s.id = rd.[sid]\n"
                    + "where rd.rid = ?";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, rid);
            rs = ps.executeQuery();
            while (rs.next()) {
                Skill p = new Skill(rs.getInt(1), rs.getString(2), rs.getInt(3));
                list.add(p);
            }
        } catch (Exception ex) {
            Logger.getLogger(SkillDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public int updateRequest(String title, String deadlineDate, String deadlineHour, String rid) throws Exception {
        int res = 0;
        String sql = "UPDATE [dbo].[Request]\n"
                + "   SET \n"
                + "      [Title] = ?      \n"
                + "      ,[deadlineDate] = ?\n"
                + "      ,[deadlineHour] = ?\n"
                + "      \n"
                + " WHERE id = ?";
        conn = new DBContext().getConnection();
        ps = conn.prepareStatement(sql);
        ps.setString(1, title);
        ps.setString(2, deadlineDate);
        ps.setString(3, deadlineHour);
        ps.setString(4, rid);
        res = ps.executeUpdate();
        return res;
    }

    public int updateReqDetail(String rid, String newSid, String oldSid, String type) throws SQLException {
        int res = 0;
        String sql = "";
        if (type.equals("update")) {
            sql = "UPDATE [dbo].[RequestDetail]\n"
                    + "SET [sid] =" + newSid + "\n"
                    + "WHERE rid = " + rid + " and sid = " + oldSid + "";
        } else {
            sql = "Insert into RequestDetail values(" + rid + "," + newSid + ")";
        }
        ps = conn.prepareStatement(sql);
        res = ps.executeUpdate();
        return res;
    }

    public int deleteReqDetail(String rid) throws Exception {
        int res = 0;
        String sql = "delete from RequestDetail where rid =?";
        conn = new DBContext().getConnection();
        ps = conn.prepareStatement(sql);
        ps.setString(1, rid);
        res = ps.executeUpdate();
        return res;
    }

    public List<Request1> getAllRequest() {
        List<Request1> list = new ArrayList<>();
        try {
            String sql = "select [id], [from], [Title], [DateReq], [status] from Request";
            Connection conn = new DBContext().getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Request1 r = new Request1();
                r.setId(rs.getInt(1));
                r.setFrom(rs.getString(2));
                r.setTitle(rs.getString(3));
                r.setStatus(rs.getInt(5));
                r.setDateReq(rs.getDate(4));

                list.add(r);
            }
        } catch (Exception ex) {
            Logger.getLogger(RequestDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return list;
    }

    public List<Request1> getByStatusAndDate(int status, Date start, Date end) {
        List<Request1> list = new ArrayList<>();
        try {
            String sql = "select [id], [from], [Title], [DateReq], [status] from Request\n"
                    + "  where status = ?\n"
                    + "  and DateReq >= ? and DateReq <= ?";
            Connection conn = new DBContext().getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, status);
            ps.setDate(2, start);
            ps.setDate(3, end);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Request1 r = new Request1();
                r.setId(rs.getInt(1));
                r.setFrom(rs.getString(2));
                r.setTitle(rs.getString(3));
                r.setStatus(rs.getInt(5));
                r.setDateReq(rs.getDate(4));

                list.add(r);
            }
        } catch (Exception ex) {
            Logger.getLogger(RequestDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return list;
    }
//    public List<Request1> getByStatusAndDate(int status , String start, String end) {
//        List<Request1> list = new ArrayList<>();
//        try {
//            String sql = "select [id], [from], [Title], [DateReq], [status] from Request\n"
//                    + "  where status = ?\n"
//                    + "  and DateReq >= ? and DateReq <= ?";
//            Connection conn = new DBContext().getConnection();
//            PreparedStatement ps = conn.prepareStatement(sql);
//            ps.setInt(1, status);
//            ps.setString(2, start);
//            ps.setString(3, end);
//
//            ResultSet rs = ps.executeQuery();
//            while (rs.next()) {
//                Request1 r = new Request1();
//                r.setId(rs.getInt(1));
//                r.setFrom(rs.getString(2));
//                r.setTitle(rs.getString(3));
//                r.setStatus(rs.getInt(5));
//                r.setDateReq(rs.getDate(4));
//
//                list.add(r);
//            }
//        } catch (Exception ex) {
//            Logger.getLogger(RequestDAO.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        return list;
//    }

    public List<Request1> searchByName(String txtSearch) {
        List<Request1> list = new ArrayList<>();
        try {
            String sql = "select [id], [from], [Title], [status],[DateReq] from Request\n"
                    + "where [from] like ?";
            Connection conn = new DBContext().getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, "%" + txtSearch + "%");

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Request1 r = new Request1();
                r.setId(rs.getInt(1));
                r.setFrom(rs.getString(2));
                r.setTitle(rs.getString(3));
                r.setStatus(rs.getInt(4));
                r.setDateReq(rs.getDate(5));
                list.add(r);
            }
        } catch (Exception ex) {
            Logger.getLogger(RequestDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return list;
    }

    public List<Request1> getListByPage(List<Request1> list, int start, int end) {
        ArrayList<Request1> a = new ArrayList<>();
        for (int i = start; i < end; i++) {
            a.add(list.get(i));
        }
        return a;
    }

    public static void main(String[] args) throws Exception {
        RequestDAO d = new RequestDAO();
        // List<Request1> lr = d.getByStatusAndDate(1, '2022-6-1', '2022-10-23');

        //int a = d.getTotalReq("Hoanghai");
//        SimpleDateFormat a = new SimpleDateFormat("yyyy-mm-dd");
//        long b= ;
//        java.sql.Date ms =new java.sql.Date(0);
//
//        List<Request1> lr = d.getRequestStart();
//        for (Request1 r : lr) {
//            System.out.println(r);
//            
//        }
//        List<Skill> list = d.getSkillby;
//        for (Skill s : list) {
//            System.out.println(s);
//        }
//    ArrayList<Request> list = d.pageRequest("Hoanghai", 0);
//        for (Request r : list) {
//            System.out.println(r);
//        }
    }
}
