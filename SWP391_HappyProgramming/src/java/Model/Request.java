/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author thuy huong
 */
public class Request {

    private int id;
    private String from;
    private String to;
    private String title;
    private String datereq;
    private String deadlineDate;
    private int deadlineHour;
    private int status;
    private String skill;

    public Request() {
    }
    
    public Request(String from, String title, int status, String skill) {
        this.from = from;
        this.title = title;
        this.status = status;
        this.skill = skill;
    }


    public String getSkill() {
        return skill;
    }

    public void setSkill(String skill) {
        this.skill = skill;
    }


    public Request(int id, String from, String to, String title, String datereq, String deadlineDate, int deadlineHour, int status) {
        this.id = id;
        this.from = from;
        this.to = to;
        this.title = title;
        this.datereq = datereq;
        this.deadlineDate = deadlineDate;
        this.deadlineHour = deadlineHour;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDatereq() {
        return datereq;
    }

    public void setDatereq(String datereq) {
        this.datereq = datereq;
    }

    public String getDeadlineDate() {
        return deadlineDate;
    }

    public void setDeadlineDate(String deadlineDate) {
        this.deadlineDate = deadlineDate;
    }

    public int getDeadlineHour() {
        return deadlineHour;
    }

    public void setDeadlineHour(int deadlineHour) {
        this.deadlineHour = deadlineHour;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override

//    public  String toString() {
//        return "Request{" + "id=" + id + ", from=" + from + ", to=" + to + ", title=" + title + ", dateRe=" + dateRe + ", deadlineDate=" + deadlineDate + ", deadHour=" + deadHour + ", status=" + status + ", skill=" + skill + '}';

    public String toString() {
        return "Request{" + "id=" + id + ", from=" + from + ", to=" + to + ", title=" + title + ", datereq=" + datereq + ", deadlineDate=" + deadlineDate + ", deadlineHour=" + deadlineHour + ", status=" + status + '}';
    }

   
    

}
