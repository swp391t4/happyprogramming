/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.Date;
import java.sql.Timestamp;

/**
 *
 * @author kingo
 */
public class CommentAndRate {
    Timestamp date; 
    String from;
    String to;
    String content;
    int star;

    public CommentAndRate() {
    }

    public CommentAndRate(Timestamp date, String from, String to, String content, int star) {
        this.date = date;
        this.from = from;
        this.to = to;
        this.content = content;
        this.star = star;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getStar() {
        return star;
    }

    public void setStar(int star) {
        this.star = star;
    }

    @Override
    public String toString() {
        return "CommentAndRate{" + "date=" + date + ", from=" + from + ", to=" + to + ", content=" + content + ", star=" + star + '}';
    }
    
}
