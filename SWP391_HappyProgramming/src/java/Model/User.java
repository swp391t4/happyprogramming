/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.Date;

/**
 *
 * @author kingo
 */
public class User {

    private String username;
    private String password;
    private String email;
    private String name;
    private int gender;
    private String phone;
    private String address;
    private String DOB;
    private String img;
    private String serviceDesc;
    private String archivementDesc;
    private String profession;
    private String professionIntro;
    private int role;

    private int Star;

    public int getStar() {
        return Star;
    }

    public void setStar(int Star) {
        this.Star = Star;
    }

    public User(int Star) {
        this.Star = Star;
    }

    public User() {

    }

    public User(String username, String password, String email, String name, int gender, String phone, String address, String DOB, String img, String serviceDesc, String archivementDesc, String profession, String professionIntro, int role) {

        this.username = username;
        this.password = password;
        this.email = email;
        this.name = name;
        this.gender = gender;
        this.phone = phone;
        this.address = address;
        this.DOB = DOB;
        this.img = img;
        this.serviceDesc = serviceDesc;
        this.archivementDesc = archivementDesc;
        this.profession = profession;
        this.professionIntro = professionIntro;
        this.role = role;
    }

    public User(String string, String string0, String string1, String string2, int aInt, String string3, String string4, Date date, String string5, String string6, String string7, String string8, String string9, String string10, int aInt0) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getServiceDesc() {
        return serviceDesc;
    }

    public void setServiceDesc(String serviceDesc) {
        this.serviceDesc = serviceDesc;
    }

    public String getArchivementDesc() {
        return archivementDesc;
    }

    public void setArchivementDesc(String archivementDesc) {
        this.archivementDesc = archivementDesc;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getProfessionIntro() {
        return professionIntro;
    }

    public void setProfessionIntro(String professionIntro) {
        this.professionIntro = professionIntro;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "User{" + "username=" + username + ", password=" + password + ", email=" + email + ", name=" + name + ", gender=" + gender + ", phone=" + phone + ", address=" + address + ", DOB=" + DOB + ", img=" + img + ", serviceDesc=" + serviceDesc + ", archivementDesc=" + archivementDesc + ", profession=" + profession + ", professionIntro=" + professionIntro + ", Role=" + role + '}';
    }

}
