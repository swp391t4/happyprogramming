/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.Date;

/**
 *
 * @author laptop 2019
 */
public class Request1 {
     private int id;
    private String from;
    private String to;
    private String title;
    private Date dateReq;
    private Date deadlineDate;
    private int deadlineHour;
    private int status;
    
    public Request1(){
        
    }

    public Request1(int id, String from, String to, String title, Date dateReq, Date deadlineDate, int deadlineHour, int status) {
        this.id = id;
        this.from = from;
        this.to = to;
        this.title = title;
        this.dateReq = dateReq;
        this.deadlineDate = deadlineDate;
        this.deadlineHour = deadlineHour;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getDateReq() {
        return dateReq;
    }

    public void setDateReq(Date dateReq) {
        this.dateReq = dateReq;
    }

    public Date getDeadlineDate() {
        return deadlineDate;
    }

    public void setDeadlineDate(Date deadlineDate) {
        this.deadlineDate = deadlineDate;
    }

    public int getDeadlineHour() {
        return deadlineHour;
    }

    public void setDeadlineHour(int deadlineHour) {
        this.deadlineHour = deadlineHour;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Request1{" + "id=" + id + ", from=" + from + ", to=" + to + ", title=" + title + ", dateReq=" + dateReq + ", deadlineDate=" + deadlineDate + ", deadlineHour=" + deadlineHour + ", status=" + status + '}';
    }

    
}
