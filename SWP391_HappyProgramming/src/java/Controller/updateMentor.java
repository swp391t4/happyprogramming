/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DAO.RequestDAO;
import DAO.SkillDAO;
import DAO.UserDAO;
import Model.Skill;
import Model.User;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

/**
 *
 * @author thuy huong
 */
@MultipartConfig
public class updateMentor extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet updateMentor</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet updateMentor at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        PrintWriter out = response.getWriter();
        RequestDAO r = new RequestDAO();
        ArrayList<Skill> list = r.getSkill();
        UserDAO dao = new UserDAO();
        HttpSession session = request.getSession();
        String username =(String) session.getAttribute("username");
        User u = dao.getUserByUsername(username);
        SkillDAO sd = new SkillDAO();
        RequestDAO rd = new RequestDAO();
        ArrayList<Skill> listskill = (ArrayList<Skill>) sd.getAllSkills();
        ArrayList<Skill> listSkillMentor = (ArrayList<Skill>) sd.getAllSkillsMentor(username);
             for (int i = 0; i < listSkillMentor.size(); i++) {
            for (int j = 0; j < listskill.size(); j++) {
                if(listSkillMentor.get(i).getId()==listskill.get(j).getId()){
                    listskill.get(j).setStatus(3);
                }
            }      
        }
        request.setAttribute("listSM", listskill);
        session.setAttribute("listSkill", list);
        request.setAttribute("mentorn", u);
//        out.print(u.getImg());
        request.getRequestDispatcher("UpdateCVofMentor.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        response.setContentType("image/jpeg"); 
        String username = request.getParameter("username");
        String name = request.getParameter("name");
        int gender = Integer.parseInt(request.getParameter("gender"));
        String dob = request.getParameter("dob");
        Part file = request.getPart("img");
        String imgname = file.getSubmittedFileName();
        String uploadPath = "img/Avatar/" + imgname;
        try {
            FileOutputStream fos = new FileOutputStream(uploadPath);
            InputStream is = file.getInputStream();
            byte[] data = new byte[is.available()];
            is.read(data);
            fos.write(data);
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        String phone = request.getParameter("phone");
        String address = request.getParameter("address");
        String intro = request.getParameter("intro");
        String nghenghiep = request.getParameter("nghenghiep");
        String dichvu = request.getParameter("dichvu");
        String thanhtuu = request.getParameter("thanhtuu");
        UserDAO dao = new UserDAO();
        SkillDAO sd = new SkillDAO();
        RequestDAO rd = new RequestDAO();
        dao.UpdateCvMentor(name,gender, phone, address, dob, uploadPath, dichvu, thanhtuu, nghenghiep, intro, username);
        String [] sid = request.getParameterValues("sid");
            RequestDAO r = new RequestDAO();
        try {
            for (int i = 0; i < sid.length - 1; i++) {
                r.insertSkillMentor(username,Integer.parseInt(sid[i]));   
            }
        } catch (Exception ex) {
            Logger.getLogger(createCV.class.getName()).log(Level.SEVERE, null, ex);
        }
        request.setAttribute("messs", "Update thành công!");
        User u = dao.getUserByUsername(username);
        request.setAttribute("mentorn", u);

      request.getRequestDispatcher("UpdateCVofMentor.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
