/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DAO.RequestDAO;
import Model.Request1;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author laptop 2019
 */
public class AdminRequest extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */

            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AdminRequest</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AdminRequest at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // processRequest(request, response);

        RequestDAO dao = new RequestDAO();
        List<Request1> lists = dao.getAllRequest();

        int page, numperpage = 5;
        int size = lists.size();
        String x = request.getParameter("page");
        int num = (size % numperpage == 0 ? (size / numperpage) : ((size / numperpage) + 1));

        if (x == null) {
            page = 1;
        } else {
            page = Integer.parseInt(x);
        }
        int start, end;
        start = (page - 1) * numperpage;
        end = Math.min(page * numperpage, size);

        List<Request1> list = dao.getListByPage(lists, start, end);

        request.setAttribute("page", page);
        request.setAttribute("num", num);
        request.setAttribute("lists", list);
        request.getRequestDispatcher("ListOfRequest.jsp").forward(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // processRequest(request, response);
        int status = Integer.parseInt(request.getParameter("status"));
        Date startDate = Date.valueOf(request.getParameter("StartD"));
        Date endDate = Date.valueOf(request.getParameter("EndD"));

        RequestDAO dao = new RequestDAO();
                List<Request1> lists = dao.getAllRequest();

        List<Request1> list1 = dao.getByStatusAndDate(status, startDate, endDate);
//        int page, numperpage = 5;
//        int size = lists.size();
//        String x = request.getParameter("page");
//        int num = (size % numperpage == 0 ? (size / numperpage) : ((size / numperpage) + 1));
//
//        if (x == null) {
//            page = 1;
//        } else {
//            page = Integer.parseInt(x);
//        }
//        int start, end;
//        start = (page - 1) * numperpage;
//        end = Math.min(page * numperpage, size);
//
//        List<Request1> list = dao.getListByPage(lists, start, end);
//
//        request.setAttribute("page", page);
//        request.setAttribute("num", num);
        request.setAttribute("lists", list1);
        request.setAttribute("status", status);
        request.setAttribute("start", startDate);
        request.setAttribute("end", endDate);
        request.getRequestDispatcher("ListOfRequest.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
