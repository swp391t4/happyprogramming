/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DAO.RequestDAO;
import Model.Request;
import Model.Skill;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author thuy huong
 */
@WebServlet(name = "viewl", urlPatterns = {"/viewl"})
public class viewl extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            HttpSession session = request.getSession();
            String username = (String)session.getAttribute("username");
            RequestDAO r = new RequestDAO();
            ArrayList<String> listSkill = new ArrayList();

            List<Skill> listRequest = r.getlistReqInviting(username);
            List<Skill> lists = new ArrayList();
            for (int i = 0; i < listRequest.size(); i++) {
                String strSkill = "";
                lists = r.getSkillbyMentor(listRequest.get(i).getId());
                for (int j = 0; j < lists.size(); j++) {
                    strSkill += lists.get(j).getName() + " ";
                    listSkill.add(strSkill);
                }
            }
            int totalReq = r.getTotalReq(username);
            int endPage = totalReq / 3;
            if (totalReq % 3 != 0) {
                endPage++;
            }
            String indexP = request.getParameter("index");
            if(indexP == null){
                indexP = "1";
            }
            int index = Integer.parseInt(indexP);
            ArrayList<Request> listP = r.pageRequest(username, index);
            request.setAttribute("listreq", listRequest);
            request.setAttribute("listSkill", listSkill);
            request.setAttribute("lists", lists);
            request.setAttribute("endP", endPage);
            request.setAttribute("listP", listP);
            String chapnhan = request.getParameter("chapnhan");
            String tuchoi = request.getParameter("tuchoi");
            out.print(listP.size());
            if (chapnhan != null) {
                request.getRequestDispatcher("ListInvitingRequest.jsp").forward(request, response);
            }
            if (tuchoi != null) {
                request.getRequestDispatcher("ListInvitingRequest.jsp").forward(request, response);
            }
            //   out.print(listRequest.size());
            request.getRequestDispatcher("ListInvitingRequest.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(viewl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(viewl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
