/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DAO.RequestDAO;
import DAO.SkillDAO;
import Model.Request;
import Model.Skill;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author kingo
 */
public class UpdateController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        try (PrintWriter out = response.getWriter()) {
            String service = request.getParameter("obj");
            SkillDAO skd = new SkillDAO();
            RequestDAO rd = new RequestDAO();
//            out.print(service==null);
            
            if(service == null){
                service = "default";
            }
            
            if(service.equals("default")){
                response.sendRedirect("home");
            }
            
            if(service.equals("request")){
                String submit = request.getParameter("submit");
                String rid = request.getParameter("rid");
                if(submit == null){
                    ArrayList<Skill> listSkillInSystem = (ArrayList<Skill>) skd.getAllSkills();
                    Request req = rd.getRequestByID(rid);
                    ArrayList<Skill> listSkillInReq = (ArrayList<Skill>) rd.getSkill_Req(rid);
                    request.setAttribute("req", req);
                    request.setAttribute("lsiS", listSkillInSystem);
                    request.setAttribute("lsiR", listSkillInReq);
                    request.setAttribute("rid", rid);
                    request.getRequestDispatcher("UpdateRequest.jsp").forward(request, response);
                } else{
                    String[] sid = request.getParameterValues("sid");
//                    for (String s : sid) {
//                        out.print("<p>"+s.split(",")[1]+"</p>");
//                    }
                    String title = request.getParameter("title");
                    String deadlineDate = request.getParameter("deadlineDate");
                    String deadlineHour = request.getParameter("deadlineHour");
                    int changeReq = rd.updateRequest(title, deadlineDate, deadlineHour, rid);
                    int changeReqDetail = 0;
                    int countChangeSkill = 0;
                    for (String s : sid) {
                        out.print("<p> start w"+s.startsWith(",")+"</p>");
                        if(s.contains(",")){
                            countChangeSkill++;
                        }
                        if(s.startsWith(",")){
                            out.print("<p> s1 "+s.split(",")[1]+"</p>");
                            changeReqDetail += rd.updateReqDetail(rid, s.split(",")[1], "", "add");
                        } else if(s.split(",").length==2){
                            out.print("<p> s1 "+s.split(",")[1]+"</p>");
                            changeReqDetail += rd.updateReqDetail(rid, s.split(",")[1], s.split(",")[0], "update");
                        }
                    }
                    out.print("<p> skill"+countChangeSkill+"</p>");
                    out.print("<p> reqd"+changeReqDetail+"</p>");
                    out.print("<p> req"+changeReq+ rid+"</p>");
                    
                    if(changeReq>0 || changeReqDetail==countChangeSkill){
                        request.setAttribute("messs", "Update request successful");
                    } else{
                        request.setAttribute("mess", "Update error please try agian!");
                    }
                    request.getRequestDispatcher("UpdateRequest.jsp").forward(request, response);
                }
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(UpdateController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(UpdateController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
