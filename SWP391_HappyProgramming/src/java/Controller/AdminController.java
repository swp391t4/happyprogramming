/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DAO.SkillDAO;
import DAO.UserDAO;
import Model.Skill;
import Model.User;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.ParseConversionEvent;

/**
 *
 * @author kingo
 */
public class AdminController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("utf-8");

        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            String service = request.getParameter("action");

            if (service == null) {
                service = "default";
            }

            if (service.equals("default")) {
                response.sendRedirect("home");
            }

            if (service.equals("createSkill")) {
                String submit = request.getParameter("submit");
                //out.print("ádfghjk");
                if (submit == null) {
                    request.getRequestDispatcher("createSkills.jsp").forward(request, response);
                } else {
                    String namesk = request.getParameter("namesk");
                    int status = Integer.parseInt(request.getParameter("status"));
                    SkillDAO skd = new SkillDAO();
                    Skill sk = skd.checkSKillName(namesk);
                    if (sk == null) {
                        skd.addNewSkill(namesk, status);
                        request.setAttribute("messs", "Thêm thành công!");
                        request.getRequestDispatcher("createSkills.jsp").forward(request, response);

                    } else {
                        request.setAttribute("mess", "Kỹ năng này đã tồn tại!");
                        out.print("cvbn");
                        request.getRequestDispatcher("createSkills.jsp").forward(request, response);
                    }
                }
            }

            if (service.equals("updateSkill")) {
                String submit = request.getParameter("submit");
                if (submit == null) {
                    int id = Integer.parseInt(request.getParameter("ID"));
                    SkillDAO skd = new SkillDAO();
                    Skill sk = skd.getSkillById(id);
                    request.setAttribute("sk", sk);
                    request.getRequestDispatcher("updateSkill.jsp").forward(request, response);
                } else {
                    int IDsk = Integer.parseInt(request.getParameter("IDsk"));
                    String namesk = request.getParameter("namesk");
                    int status = Integer.parseInt(request.getParameter("status"));
                    SkillDAO skd = new SkillDAO();
//                    Skill sk = skd.checkSKillName(namesk);
//                    if (sk == null) {
                    Skill skn = new Skill(IDsk, namesk, status);
                    skd.updateSkill(skn);
//                        request.setAttribute("messs", "Thêm thành công!");
                    response.sendRedirect("admin?action=viewSkill");
//                    } else {
//                        request.setAttribute("mess", "Kỹ năng này đã tồn tại!");
//                        request.setAttribute("IDsk", IDsk);
//                        request.setAttribute("namesk", namesk);
//                        request.setAttribute("status", status);
//                        request.getRequestDispatcher("updateSkill.jsp").forward(request, response);
//                    }
                }
            }
            if (service.equals("deleteSkill")) {
                int id = Integer.parseInt(request.getParameter("ID"));
                SkillDAO skd = new SkillDAO();
                skd.delete(id);
                response.sendRedirect("admin?action=viewSkill");
            }

            if (service.equals("viewSkill")) {
                SkillDAO skd = new SkillDAO();
                String curP = request.getParameter("page");
                if (curP == null) {
                    curP = "1";
                }

                int totalSkill = skd.getTotalSkill();
                int totalP = totalSkill / 2;
                if (totalSkill % 2 != 0) {
                    totalP = totalP + 1;
                }
                int startP = 0;
                int endP = 0;
                if (totalP <= 5) {
                    startP = 1;
                    endP = totalP;
                } else {
                    startP = Integer.parseInt(curP) - 2;
                    endP = Integer.parseInt(curP) + 2;
                    if (Integer.parseInt(curP) <= 2) {
                        startP = 1;
                        endP = 5;
                    }
                    if (totalP - Integer.parseInt(curP) <= 2) {
                        endP = totalP;
                        startP = totalP - 4;
                    }
                }
                List<Skill> list = skd.pagingSkill(Integer.parseInt(curP), 4);
                request.setAttribute("list", list);
                request.setAttribute("startP", startP);
                request.setAttribute("endP", endP);
                request.setAttribute("curP", curP);
                request.setAttribute("totalP", totalP);
                request.getRequestDispatcher("ViewAllSkillAdmin.jsp").forward(request, response);
            }
            if (service.equals("paginationListSkill")) {
                SkillDAO skd = new SkillDAO();

                String[] sid = request.getParameterValues("sid");
                String curP = request.getParameter("index");
                ArrayList<Skill> ls = skd.pagingSkill(Integer.parseInt(curP), 4);
//                out.print(sid == null);

//                if (sid != null) {
//                    out.print(sid[0]);
//                }
////                if (sid != null) {
////                    out.print(sid[0]);
////                }
//                for (int i = 0; i < sid.length - 1; i++) {
//                    out.print("<input hidden=\"\" value=\""+sid[i]+"\" name=\"sid\">");
//                }
                for (int i = 0; i < ls.size(); i++) {
                    int idx = (i + 1) + (Integer.parseInt(curP) - 1) * 2;
                    out.println("<tr>\n"
                            + "                                                <td data-label=\"Name\" style=\"width: 100px\">\n"
                            + "                                                    <span>" + idx + "</span>\n"
                            + "                                                </td>\n"
                            + "                                                <td data-label=\"ID\">\n"
                            + "                                                    <span>" + ls.get(i).getId() + "</span>\n"
                            + "                                                </td>\n"
                            + "                                                <td data-label=\"Username\">\n"
                            + "                                                    <span>" + ls.get(i).getName() + "</span>\n"
                            + "                                                </td>\n"
                            + "                                                <td data-label=\"Status\">\n");
                    if (ls.get(i).getStatus() == 1) {
                        out.print("<span> \n"
                                + " Enable\n"
                                + " </span>");
                    } else {
                        out.print("<span> \n"
                                + " Disable\n"
                                + "</span>");
                    }
                    out.print("</td>\n"
                            + "                                                <td data-label=\"Status\">\n"
                            + "                                                    <span>\n"
                            + "                                                        <button><a href =\"admin?action=updateSkill&ID=" + ls.get(i).getId() + "\">Update</a></button>\n"
                            + "                                                    </span>\n"
                            + "                                                    <span>\n"
                            + "                                                        <button><a href =\"admin?action=deleteSkill&ID=" + ls.get(i).getId() + "\" onclick=\"return confirm('Are you sure you want to delete this item?');\">Delete</a></button>\n"
                            + "                                                    </span>\n"
                            + "                                                </td>\n"
                            + "\n"
                            + "                                            </tr>");

                }
            }

            if (service.equals("listPageSkill")) {
                String curP = request.getParameter("index");
                String totalP = request.getParameter("totalP");
                String sid = request.getParameter("sid");
                int startP = 0;
                int endP = 0;
                if (Integer.parseInt(totalP) <= 5) {
                    startP = 1;
                    endP = Integer.parseInt(totalP);
                } else {
                    startP = Integer.parseInt(curP) - 2;
                    endP = Integer.parseInt(curP) + 2;
                    if (Integer.parseInt(curP) <= 2) {
                        startP = 1;
                        endP = 5;
                    }
                    if (Integer.parseInt(totalP) - Integer.parseInt(curP) <= 2) {
                        endP = Integer.parseInt(totalP);
                        startP = Integer.parseInt(totalP) - 4;
                    }
                }
                if (Integer.parseInt(curP) != 1) {
                    out.println("<button onclick=\"Pagination( '" + (Integer.parseInt(curP) - 1) + "', '" + (Integer.parseInt(totalP)) + "')\" class=\"btn btn-outline-info\">Previous</button>");
                }
                for (int i = startP; i <= endP; i++) {
                    out.println("<button  onclick=\"Pagination( '" + i + "', '" + (Integer.parseInt(totalP)) + "')\"  class=\"btn btn-outline-info " + (Integer.parseInt(curP) == i ? "btn-outline-danger" : "") + "\">" + i + "</button>");

                }

                if (Integer.parseInt(curP) != Integer.parseInt(totalP)) {
                    out.println("<button onclick=\"Pagination( '" + (Integer.parseInt(curP) + 1) + "', '" + (Integer.parseInt(totalP)) + "')\" class=\"btn btn-outline-info\">Next</button>");
                }
            }

            if (service.equals("statisticMentee")) {
                UserDAO ud = new UserDAO();
                String curP = request.getParameter("page");
                if (curP == null) {
                    curP = "1";
                }
                int totalMentee = ud.getTotalMentee();
                int totalP = totalMentee / 2;
                if (totalMentee % 2 != 0) {
                    totalP = totalP + 1;
                }
                int startP = 0;
                int endP = 0;
                if (totalP <= 5) {
                    startP = 1;
                    endP = totalP;
                } else {
                    startP = Integer.parseInt(curP) - 2;
                    endP = Integer.parseInt(curP) + 2;
                    if (Integer.parseInt(curP) <= 2) {
                        startP = 1;
                        endP = 5;
                    }
                    if (totalP - Integer.parseInt(curP) <= 2) {
                        endP = totalP;
                        startP = totalP - 4;
                    }
                }
                ArrayList<User> listMentee = ud.pagingMentee(Integer.parseInt(curP));
                int totalH = ud.getTotalHoursOfAllReq();
                int totalS = ud.getTotalOfSkillOfAllReq();
                request.setAttribute("curP", curP);
                request.setAttribute("startP", startP);
                request.setAttribute("endP", endP);
                request.setAttribute("totalP", totalP);
                request.setAttribute("listMentee", listMentee);
                request.setAttribute("totalMentee", totalMentee);
                request.setAttribute("totalH", totalH);
                request.setAttribute("totalS", totalS);
                out.print(curP);
                request.getRequestDispatcher("StatisticOfAllMentee.jsp").forward(request, response);
            }

            if (service.equals("paginationListMentee")) {
                String curP = request.getParameter("index");
                UserDAO ud = new UserDAO();
                ArrayList<User> listMentee = ud.pagingMentee(Integer.parseInt(curP));
                for (int i = 0; i < listMentee.size(); i++) {
                    int idx = (i + 1) + (Integer.parseInt(curP) - 1) * 2;
                    out.print("<tr>\n"
                            + "                                        <td>\n"
                            + "                                            <div class=\"d-flex mt-2 border-right\">\n"
                            + "                                                <div class=\"box p-2 rounded\">\n"
                            + "                                                    <span class=\"text-primary px-2 font-weight-bold\">" + idx + "</span>\n"
                            + "                                                </div>\n"
                            + "                                            </div>\n"
                            + "                                        </td>\n"
                            + "                                        <td>\n"
                            + "                                            <div class=\"d-flex flex-column\">\n"
                            + "                                                <div class=\"d-flex align-items-center\">\n"
                            + "                                                    <b class=\"pl-2\">" + listMentee.get(i).getName() + "</b>\n"
                            + "                                                </div>\n"
                            + "                                            </div>\n"
                            + "                                        </td>\n"
                            + "                                        <td>\n"
                            + "                                            <div class=\"d-flex flex-column\">\n"
                            + "                                                <div><b>" + listMentee.get(i).getUsername() + "</b></div>\n"
                            + "                                            </div>\n"
                            + "                                        </td>\n"
                            + "                                    </tr>");
                }
            }

            if (service.equals("listPageMentee")) {
                String curP = request.getParameter("index");
                String totalP = request.getParameter("totalP");
                int startP = 0;
                int endP = 0;
                if (Integer.parseInt(totalP) <= 5) {
                    startP = 1;
                    endP = Integer.parseInt(totalP);
                } else {
                    startP = Integer.parseInt(curP) - 2;
                    endP = Integer.parseInt(curP) + 2;
                    if (Integer.parseInt(curP) <= 2) {
                        startP = 1;
                        endP = 5;
                    }
                    if (Integer.parseInt(totalP) - Integer.parseInt(curP) <= 2) {
                        endP = Integer.parseInt(totalP);
                        startP = Integer.parseInt(totalP) - 4;
                    }
                }
                if (Integer.parseInt(curP) != 1) {
                    out.println("<button onclick=\"Pagination('" + (Integer.parseInt(curP) - 1) + "', '" + (Integer.parseInt(totalP)) + "')\" class=\"btn btn-outline-info\">Previous</button>");
                }
                for (int i = startP; i <= endP; i++) {
                    out.println("<button  onclick=\"Pagination('" + i + "', '" + (Integer.parseInt(totalP)) + "')\"  class=\"btn btn-outline-info " + (Integer.parseInt(curP) == i ? "btn-outline-danger" : "") + "\">" + i + "</button>");
                }

                if (Integer.parseInt(curP) != Integer.parseInt(totalP)) {
                    out.println("<button onclick=\"Pagination('" + (Integer.parseInt(curP) + 1) + "', '" + (Integer.parseInt(totalP)) + "')\" class=\"btn btn-outline-info\">Next</button>");
                }
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(EvaluationController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);

        } catch (Exception ex) {
            Logger.getLogger(AdminController.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
        public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
}

/**
 * Returns a short description of the servlet.
 *
 * @return a String containing servlet description
 */



