/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DAO.CreateRequestDAO;
import DAO.SkillDAO;
import Model.RequestDetail;
import Model.Skill;
import Model.User;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.sql.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author laptop 2019
 */
public class CreateRequestController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */

            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CreateRequestController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CreateRequestController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        SkillDAO db2 = new SkillDAO();
        List<Skill> list1 = db2.getAllSkills();
        request.setAttribute("lists", list1);
        request.getRequestDispatcher("CreateRequest.jsp").forward(request, response);
//        PrintWriter out = response.getWriter();
//        out.println("hello");
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //  processRequest(request, response);

        String title = request.getParameter("title");
        Date deadlineDate = Date.valueOf(request.getParameter("deadlineDate"));
        String deadlineHour = request.getParameter("deadlineHour");
        String cb1 = request.getParameter("cb1");
        String cb2 = request.getParameter("cb2");
        String cb3 = request.getParameter("cb3");

        int sta = 1;
//        Date dt = new Date();
//       SimpleDateFormat d = new SimpleDateFormat("yyyy-mm-dd");

        //String da = d.format(dt);
        long millis = System.currentTimeMillis();
        java.sql.Date date = new java.sql.Date(millis);

        PrintWriter out = response.getWriter();

        HttpSession ses = request.getSession();
        User a = (User) ses.getAttribute("user");

        String user = a.getUsername();

        SkillDAO db2 = new SkillDAO();
        List<Skill> list1 = db2.getAllSkills();
        // request.setAttribute("lists", list1);
        for (Skill s : list1) {
            System.out.println(s);
        }

        //try {
//            if (deadlineHour == null || title == null || deadlineDate == null || cb.length == 0) {
//                request.setAttribute("messs", "create failed");
//                response.sendRedirect("CreateRequest.jsp");
//            } else {
        CreateRequestDAO db = new CreateRequestDAO();
        db.createRequest(user, title, date, deadlineDate, deadlineHour, sta);

        CreateRequestDAO db1 = new CreateRequestDAO();
        int id;
        id = db1.getLastRequest();
        ArrayList<String> la = new ArrayList<>();
        la.add(cb1);
        la.add(cb2);
        la.add(cb3);

        for (int i = 0; i < la.size(); i++) {
            db1.createRequestDetail(id, la.get(i));
        }
 
            
        

        
        request.setAttribute("mess", "Create successfully");
        request.getRequestDispatcher("CreateRequest.jsp").forward(request, response);

    }

//        } catch (Exception e) {
//            System.out.println("loi: " + e);
//        }
    //}
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
