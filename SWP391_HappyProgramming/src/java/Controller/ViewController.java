/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DAO.ListRequestByMeDAO;
import DAO.RequestDAO;
import DAO.SkillDAO;
import DAO.UserDAO;
import Model.Request;
import Model.Request1;
import Model.Skill;
import Model.User;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.jboss.weld.servlet.SessionHolder;

/**
 *
 * @author kingo
 */
public class ViewController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        try (PrintWriter out = response.getWriter()) {
            UserDAO ud = new UserDAO();
            String service = request.getParameter("id");
            if (service == null) {
                response.sendRedirect("home");
            }

            if (service.equals("listMentor")) {
                String curP = request.getParameter("page");
                if (curP == null) {
                    curP = "1";
                }
//                String sid = request.getParameter("sid");
//                int totalMentor = ud.getTotalMentor(1);
                String[] sid = request.getParameterValues("sid");
                String[] sids = {"1", "2"};
                String strSID = "";
                for (int i = 0; i < sid.length; i++) {
                    if (i == sid.length - 1) {
                        strSID += sid[i];
                        break;
                    }
                    strSID += sid[i] + ",";
                }
                HttpSession session = request.getSession();
                session.setAttribute("sid[]", strSID);
                int totalMentor = ud.getTotalMentor(strSID);

                int totalP = totalMentor / 2;
                if (totalMentor % 2 != 0) {
                    totalP = totalP + 1;
                }
                int startP = 0;
                int endP = 0;
                if (totalP <= 5) {
                    startP = 1;
                    endP = totalP;
                } else {
                    startP = Integer.parseInt(curP) - 2;
                    endP = Integer.parseInt(curP) + 2;
                    if (Integer.parseInt(curP) <= 2) {
                        startP = 1;
                        endP = 5;
                    }
                    if (totalP - Integer.parseInt(curP) <= 2) {
                        endP = totalP;
                        startP = totalP - 4;
                    }

                }

                ArrayList<User> lm = ud.pagingMentor(Integer.parseInt(curP), strSID);
                ArrayList<Integer> listReceived = new ArrayList<>();
                ArrayList<Integer> listInvited = new ArrayList<>();
                ArrayList<Float> listRate = new ArrayList<>();
                for (User u : lm) {
                    listReceived.add(ud.getNumOfReqReceivedByUser(u.getUsername()));
                    listInvited.add(ud.getNumOfReqInvitedByUser(u.getUsername()));
                    listRate.add(ud.getAvgRateByUsername(u.getUsername()));
                }
                request.setAttribute("startP", startP);
                request.setAttribute("endP", endP);
                request.setAttribute("curP", curP);
                request.setAttribute("totalP", totalP);
                request.setAttribute("listRate", listRate);
                request.setAttribute("lm", lm);
                request.setAttribute("listReceived", listReceived);
                request.setAttribute("listInvited", listInvited);
                for (int i = 0; i < lm.size(); i++) {
                    out.println(lm.get(i).getImg() + lm.get(i).getName() + "\t" + lm.get(i).getUsername() + "\t" + listReceived.get(i) + "\t" + listInvited.get(i) + "\t" + listRate.get(i));

                }
                request.getRequestDispatcher("ViewListMentor.jsp").forward(request, response);
            }

            if (service.equals("paginationListMentor")) {
                HttpSession session = request.getSession();
                String sid = (String) session.getAttribute("sid[]");
                String curP = request.getParameter("index");
                ArrayList<User> lm = ud.pagingMentor(Integer.parseInt(curP), sid);
                ArrayList<Integer> listReceived = new ArrayList<>();
                ArrayList<Integer> listInvited = new ArrayList<>();
                ArrayList<Float> listRate = new ArrayList<>();
                for (User u : lm) {
                    listReceived.add(ud.getNumOfReqReceivedByUser(u.getUsername()));
                    listInvited.add(ud.getNumOfReqInvitedByUser(u.getUsername()));
                    listRate.add(ud.getAvgRateByUsername(u.getUsername()));
                }
                for (int i = 0; i < lm.size(); i++) {
                    out.println("<tr>\n"
                            + "                                            <td data-label=\"Name\" style=\"text-align: left\">\n"
                            + "                                                <img alt=\"...\" src=\"" + lm.get(i).getImg() + "\" class=\"avatar avatar-sm rounded-circle me-2\">\n"
                            + "                                                <a class=\"text-heading font-semibold\" href=\"#\">\n"
                            + "                                                    " + lm.get(i).getName() + "\n"
                            + "                                                </a>\n"
                            + "                                            </td>\n"
                            + "                                            <td data-label=\"Username\">\n"
                            + "                                                <span>" + lm.get(i).getUsername() + "</span>\n"
                            + "                                            </td>\n"
                            + "                                            <td data-label=\"Received Request\">\n"
                            + "                                                <span>" + listReceived.get(i) + "</span>\n"
                            + "                                            </td>\n"
                            + "                                            <td data-label=\"Invited Request\">\n"
                            + "                                                <span>" + listInvited.get(i) + "</span>\n"
                            + "                                            </td>\n"
                            + "                                            <td data-label=\"Rate\">\n"
                            + "                                                <div class=\"small-ratings\">\n"
                            //                        + "                                                    <i class=\"fa " + (listRate.get(i) == 0 ? " fa-star " : (listRate.get(i) < 0.85 ? " fa-star-half-full rating-color " : " fa-star rating-color ")) + "\"></i>\n"
                            + "                                                    <i class=\"fa " + (listRate.get(i) == 0 ? "fa-star" : (listRate.get(i) < 0.85 ? "fa-star-half-full rating-color" : "fa-star rating-color")) + "\"></i>\n"
                            + "                                                    <i class=\"fa " + (listRate.get(i) < 1.35 ? "fa-star" : (listRate.get(i) < 1.85 ? "fa-star-half-full rating-color" : "fa-star rating-color")) + "\"></i>\n"
                            + "                                                    <i class=\"fa " + (listRate.get(i) < 2.35 ? "fa-star" : (listRate.get(i) < 2.85 ? "fa-star-half-full rating-color" : "fa-star rating-color")) + "\"></i>\n"
                            + "                                                    <i class=\"fa " + (listRate.get(i) < 3.35 ? "fa-star" : (listRate.get(i) < 3.85 ? "fa-star-half-full rating-color" : "fa-star rating-color")) + "\"></i>\n"
                            + "                                                    <i class=\"fa " + (listRate.get(i) < 4.35 ? "fa-star" : (listRate.get(i) < 4.85 ? "fa-star-half-full rating-color" : "fa-star rating-color")) + "\"></i>\n"
                            + "                                                </div>\n"
                            + "                                                <span class=\"badge bg-soft-success text-success\">" + String.format("%.1f", listRate.get(i)) + " /5</span>\n"
                            + "                                                </td>\n"
                            + "                                                <td data-label=\"\">\n"
                            + "                                                    <a class=\"text-current\" href=\"#\">Gửi yêu cầu</a>\n"
                            + "                                                </td>\n"
                            + "                                                \n"
                            + "                                            </tr>");
                }
            }

            if (service.equals("listPage")) {
                String curP = request.getParameter("index");
                String totalP = request.getParameter("totalP");
                HttpSession session = request.getSession();
                String sid = (String) session.getAttribute("sid[]");
                int startP = 0;
                int endP = 0;
                if (Integer.parseInt(totalP) <= 5) {
                    startP = 1;
                    endP = Integer.parseInt(totalP);
                } else {
                    startP = Integer.parseInt(curP) - 2;
                    endP = Integer.parseInt(curP) + 2;
                    if (Integer.parseInt(curP) <= 2) {
                        startP = 1;
                        endP = 5;
                    }
                    if (Integer.parseInt(totalP) - Integer.parseInt(curP) <= 2) {
                        endP = Integer.parseInt(totalP);
                        startP = Integer.parseInt(totalP) - 4;
                    }
                }
                if (Integer.parseInt(curP) != 1) {
                    out.println("<button onclick=\"Pagination('" + sid + "', '" + (Integer.parseInt(curP) - 1) + "', '" + (Integer.parseInt(totalP)) + "')\" class=\"btn btn-outline-info\">Previous</button>");
                }
                for (int i = startP; i <= endP; i++) {
                    out.println("<button  onclick=\"Pagination('" + sid + "', '" + i + "', '" + (Integer.parseInt(totalP)) + "')\"  class=\"btn btn-outline-info " + (Integer.parseInt(curP) == i ? "btn-outline-danger" : "") + "\">" + i + "</button>");
                }

                if (Integer.parseInt(curP) != Integer.parseInt(totalP)) {
                    out.println("<button onclick=\"Pagination('" + sid + "', '" + (Integer.parseInt(curP) + 1) + "', '" + (Integer.parseInt(totalP)) + "')\" class=\"btn btn-outline-info\">Next</button>");
                }
            }

            if (service.equals("ViewAllSkill")) {
                
                SkillDAO skd = new SkillDAO();
                String curP = request.getParameter("page");
                if (curP == null) {
                    curP = "1";
                }
                int totalSkill = skd.getTotalSkill();
                int totalP = totalSkill / 2;
                if (totalSkill % 2 != 0) {
                    totalP = totalP + 1;
                }
                int startP = 0;
                int endP = 0;
                if (totalP <= 5) {
                    startP = 1;
                    endP = totalP;
                } else {
                    startP = Integer.parseInt(curP) - 2;
                    endP = Integer.parseInt(curP) + 2;
                    if (Integer.parseInt(curP) <= 2) {
                        startP = 1;
                        endP = 5;
                    }
                    if (totalP - Integer.parseInt(curP) <= 2) {
                        endP = totalP;
                        startP = totalP - 4;
                    }
                }
                List<Skill> list = skd.pagingSkill(Integer.parseInt(curP),0);
                request.setAttribute("list", list);
                request.setAttribute("startP", startP);
                request.setAttribute("endP", endP);
                request.setAttribute("curP", curP);
                request.setAttribute("totalP", totalP);
                request.getRequestDispatcher("ViewAllSkill.jsp").forward(request, response);
            }

            if (service.equals("paginationListSkill")) {
                SkillDAO skd = new SkillDAO();
                
                String[] sid = request.getParameterValues("sid");
                String curP = request.getParameter("index");
                ArrayList<Skill> ls = skd.pagingSkill(Integer.parseInt(curP),0);
//                out.print(sid == null);

//                if (sid != null) {
//                    out.print(sid[0]);
//                }
////                if (sid != null) {
////                    out.print(sid[0]);
////                }
//                for (int i = 0; i < sid.length - 1; i++) {
//                    out.print("<input hidden=\"\" value=\""+sid[i]+"\" name=\"sid\">");
//                }
                for (int i = 0; i < ls.size(); i++) {
                    int idx = (i + 1) + (Integer.parseInt(curP) - 1) * 2;
                    out.println("<tr>\n"
                            + "                                        <td data-label=\"Name\" style=\"width: 100px\" >\n"
                            + "                                                <span>" + idx + "</span>\n"
                            + "                                        </td>\n"
                            + "                                        <td data-label=\"Username\">\n"
                            + "                                                <span>" + ls.get(i).getName() + "</span>\n"
                            + "                                        </td>\n"
                            + "                                        <td data-label=\"Received Request\">\n"
                            + "                                            <span><input type=\"checkbox\" name=\"sid\" value=\"" + ls.get(i).getId() + "\"></span>\n"
                            + "                                        </td>\n"
                            + "                                    </tr>");
                }
            }

            if (service.equals("listPageSkill")) {
                String curP = request.getParameter("index");
                String totalP = request.getParameter("totalP");
                String sid = request.getParameter("sid");
                int startP = 0;
                int endP = 0;
                if (Integer.parseInt(totalP) <= 5) {
                    startP = 1;
                    endP = Integer.parseInt(totalP);
                } else {
                    startP = Integer.parseInt(curP) - 2;
                    endP = Integer.parseInt(curP) + 2;
                    if (Integer.parseInt(curP) <= 2) {
                        startP = 1;
                        endP = 5;
                    }
                    if (Integer.parseInt(totalP) - Integer.parseInt(curP) <= 2) {
                        endP = Integer.parseInt(totalP);
                        startP = Integer.parseInt(totalP) - 4;
                    }

                }
                if (Integer.parseInt(curP) != 1) {
                    out.println("<button onclick=\"Pagination( '" + (Integer.parseInt(curP) - 1) + "', '" + (Integer.parseInt(totalP)) + "')\" class=\"btn btn-outline-info\">Previous</button>");
                }
                for (int i = startP; i <= endP; i++) {
                    out.println("<button  onclick=\"Pagination( '" + i + "', '" + (Integer.parseInt(totalP)) + "')\"  class=\"btn btn-outline-info " + (Integer.parseInt(curP) == i ? "btn-outline-danger" : "") + "\">" + i + "</button>");

                }

                if (Integer.parseInt(curP) != Integer.parseInt(totalP)) {
                    out.println("<button onclick=\"Pagination( '" + (Integer.parseInt(curP) + 1) + "', '" + (Integer.parseInt(totalP)) + "')\" class=\"btn btn-outline-info\">Next</button>");
                }
            }
            if (service.equals("statisticRequest")) {
                RequestDAO rd = new RequestDAO();
                int rtotal = rd.getTotalReq();
                int rtotalh = rd.getTotalHourReq();
                int rtotalm = rd.getTotalMentorForMentee();
                List<Request> list = new ArrayList<>();
                list = rd.getAllReq();
                // out.print(list.get(0));
                request.setAttribute("list", list);
                request.setAttribute("rtotal", rtotal);
                request.setAttribute("rtotalh", rtotalh);
                request.setAttribute("rtotalm", rtotalm);
//                out.print(rtotal);
//                out.print(rtotalh);
//                out.print(rtotalm);
                request.getRequestDispatcher("statisticRequestByMe.jsp").forward(request, response);
            }

            if (service.equals("statisticRequestOfMentor")) {
                HttpSession session = request.getSession();
                String username = session.getAttribute("username").toString();
                RequestDAO reqdao = new RequestDAO();

                String indexPage = request.getParameter("index");
                if (indexPage == null) {
                    indexPage = "1";
                }
                List<Request> listPage = new ArrayList<>();
                List<Request> list = new ArrayList<>();
                int index = Integer.parseInt(indexPage);

                list = reqdao.getallStatusRequest(username);
                listPage = reqdao.pagingRequest(username, index);
                int countP = reqdao.getTotal(username);
                int endPage = countP / 4;
                if (countP % 4 != 0) {
                    endPage++;
                }
                request.setAttribute("listPage", listPage);
                request.setAttribute("endP", endPage);
                int numberofrequest = list.size();
                double percentofaccepted = 0;
                double percentofcanceled = 0;
                double count = 0;
                double count2 = 0;
                for (Request o : list) {
                    if (o.getStatus() == 2) {
                        count++;
                    }
                    if (o.getStatus() == 4) {
                        count2++;
                    }
                }
                percentofcanceled = (count2 / numberofrequest) * 100;
                percentofaccepted = (count / numberofrequest) * 100;

                request.setAttribute("accepted", (int) (percentofaccepted));
                request.setAttribute("canceled", (int) (percentofcanceled));
                request.getRequestDispatcher("ViewAllRequestOfMentor.jsp").forward(request, response);
            }

            if (service.equals("listRequestOfMentee")) {
                HttpSession session = request.getSession();
                User a = (User) session.getAttribute("user");
                session.removeAttribute("messEvaluation");
                String username = a.getUsername();
                ListRequestByMeDAO dao = new ListRequestByMeDAO();
                List<Request1> list = dao.ListRequest(username);
                request.setAttribute("listr", list);
                request.getRequestDispatcher("ListRequestOfMentee.jsp").forward(request, response);
            }

            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(ViewController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {           
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(ViewController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
