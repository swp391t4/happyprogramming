/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DAO.RequestDAO;
import DAO.UserDAO;
import Model.Request;
import Model.User;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Admin
 */
public class ViewAllMentor extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ViewAllMentor</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ViewAllMentor at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("utf-8");
        HttpSession session = request.getSession();
        String name = request.getParameter("name");
        session.setAttribute("name", name);
        UserDAO dao = new UserDAO();
        RequestDAO reqdao = new RequestDAO();
        if (name == null) {
            String indexPage = request.getParameter("index");
            if (indexPage == null) {
                indexPage = "1";
            }
            int index = Integer.parseInt(indexPage);
            List<User> listPage = dao.pagingUser(index);
            List<String> accept = new ArrayList<>();
            List<String> cancel = new ArrayList<>();
            List<String> complete = new ArrayList<>();
            int numaccept = 0;
            int numcancel = 0;
            double numcomplete = 0;
            double percentofcomplete = 0;
            for (int i = 0; i < listPage.size(); i++) {
                List<Request> req = reqdao.getReqInviting(listPage.get(i).getUsername());
                for (int y = 0; y < req.size(); y++) {

                    if (req.get(y).getStatus() == 2) {

                        numaccept++;
                    }
                    if (req.get(y).getStatus() == 4) {
                        numcancel++;
                    }
                    if (req.get(y).getStatus() == 3) {
                        numcomplete++;
                    }

                }
                String num1 = String.valueOf(numaccept);
                accept.add(num1);
                numaccept = 0;
                String num2 = String.valueOf(numcancel);
                cancel.add(num2);
                numcancel = 0;
                double percentofcompleted = 0;
                if (req.size() != 0) {
                    percentofcompleted = (numcomplete / req.size()) * 100;
                    String num3 = String.valueOf(percentofcompleted);
                    complete.add(num3);
                    session.setAttribute("complete", complete);
                    numcomplete = 0;

                } else {
                    percentofcompleted = 0;
                    String num3 = String.valueOf(percentofcompleted);
                    complete.add(num3);
                    numcomplete = 0;
                    session.setAttribute("complete", complete);

                }
                session.setAttribute("accept", accept);
                session.setAttribute("cancel", cancel);
            }

            session.setAttribute("listMentorP", listPage);
//            session.setAttribute("list", list);

            int count = dao.getTotal(3);
            int endPage = count / 3;
            if (count % 3 != 0) {
                endPage++;
            }
            session.setAttribute("endMentorP", endPage);
            request.getRequestDispatcher("ViewAllMentor.jsp").forward(request, response);
        } else {
            String indexPage = request.getParameter("index");
            if (indexPage == null) {
                indexPage = "1";
            }
            int index = Integer.parseInt(indexPage);
            List<User> listPage = dao.getallUserBySearch(name, index);
            List<String> accept = new ArrayList<>();
            List<String> cancel = new ArrayList<>();
            List<String> complete = new ArrayList<>();
            int numaccept = 0;
            int numcancel = 0;
            double numcomplete = 0;
            double percentofcomplete = 0;
            for (int i = 0; i < listPage.size(); i++) {
                List<Request> req = reqdao.getReqInviting(listPage.get(i).getUsername());
                for (int y = 0; y < req.size(); y++) {

                    if (req.get(y).getStatus() == 2) {

                        numaccept++;
                    }
                    if (req.get(y).getStatus() == 4) {
                        numcancel++;
                    }
                    if (req.get(y).getStatus() == 3) {
                        numcomplete++;
                    }

                }
                String num1 = String.valueOf(numaccept);
                accept.add(num1);
                numaccept = 0;
                String num2 = String.valueOf(numcancel);
                cancel.add(num2);
                numcancel = 0;
                double percentofcompleted = 0;
                if (req.size() != 0) {
                    percentofcompleted = (numcomplete / req.size()) * 100;
                    String num3 = String.valueOf(percentofcompleted);
                    complete.add(num3);
                    session.setAttribute("complete1", complete);
                    numcomplete = 0;

                } else {
                    percentofcompleted = 0;
                    String num3 = String.valueOf(percentofcompleted);
                    complete.add(num3);
                    numcomplete = 0;
                    session.setAttribute("complete1", complete);

                }
                session.setAttribute("accept1", accept);
                session.setAttribute("cancel1", cancel);
            }

            session.setAttribute("listP1", listPage);

            int count = dao.getTotalMentorByName(3, name);
            int endPage = count / 3;
            if (count % 3 != 0) {
                endPage++;
            }
            session.setAttribute("endP1", endPage);
            request.getRequestDispatcher("ViewAllMentor.jsp").forward(request, response);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
