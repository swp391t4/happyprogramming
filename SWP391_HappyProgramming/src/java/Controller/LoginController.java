/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DAO.UserDAO;
import Model.User;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author kingo
 */
public class LoginController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    public static String rdPass() {
        Random rd = new Random();
        StringBuilder sb = new StringBuilder();
        String a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789~!@#$%^&*()";
        for (int i = 0; i < 6; i++) {
            char c = a.charAt(rd.nextInt(72));
            sb.append(c);
        }
        return sb.toString();
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        try (PrintWriter out = response.getWriter()) {
            UserDAO ud = new UserDAO();
            String service = request.getParameter("go");

            if (service == null) {
                request.getRequestDispatcher("Login.jsp").forward(request, response);
            }

            // Đặt lại mật khẩu
            if (service.equals("resetPassword")) {
                String submit = request.getParameter("submit");
                if (submit == null) { // Người dùng truy cập bằng đường link/Get
                    request.setAttribute("ms", "");
                } else { // Khi người dùng click "Đặt lại mật khẩu"
                    SendMail sm = new SendMail();
                    String username = request.getParameter("username");
                    String email = request.getParameter("email");
                    User user = ud.getUserByEmail(username, email);
                    if (user.getUsername() != null) {
                        String newPass = rdPass();
                        sm.send(email, "ResetPassword", "New password: " + newPass, "he15124399@gmail.com", "pskqnpkwbgirlhoh");
                        ud.changePassword(username, newPass);
                        request.setAttribute("stt", "1");
                        request.setAttribute("ms", "Đặt lại mật khẩu thành công! Kiểm tra email!");
                    } else {
                        request.setAttribute("stt", "");
                        request.setAttribute("ms", "Không tìm thấy thông tin tài khoản!");
                    }
                }
                request.getRequestDispatcher("ResetPass.jsp").forward(request, response);
            }

            // Đổi mật khẩu
            if (service.equals("changePassword")) {
                String submit = request.getParameter("submit");
                if (submit == null) { // Người dùng truy cập bằng đường link/Get
                    request.setAttribute("ms", "");
                    request.getRequestDispatcher("ChangePassword.jsp").forward(request, response);
                } else { // Khi người dùng click "Đổi mật khẩu"
                    String username = request.getParameter("username");
                    String oldP = request.getParameter("oldPass");
                    String newP = request.getParameter("newPass");
                    User user = ud.login(username, oldP);
                    if (user.getUsername() != null) {
                        ud.changePassword(username, newP);
                        request.setAttribute("ms", "Đổi mật khẩu thành công!");
                    } else {
                        request.setAttribute("ms", "Thông tin tài khoản không hợp lệ!");
                    }
                    request.getRequestDispatcher("ChangePassword.jsp").forward(request, response);
                }
            }
            //login vao he thong
            if (service.equals("login")) {

                String usern = request.getParameter("username");
                String passw = request.getParameter("password");
                String remember = request.getParameter("remember");
                User user = ud.login(usern, passw);
                if (user.getUsername() == null && user.getPassword() == null) {
                    request.setAttribute("mess", "Incorrect Username or Password");
                    request.getRequestDispatcher("Login.jsp").forward(request, response);
                }
                if (user.getUsername() != null) {

                    Cookie userC = new Cookie("username", usern);
                    Cookie passC = new Cookie("password", passw);
                    if (remember != null) {
                        userC.setMaxAge(60 * 60 * 24);
                        passC.setMaxAge(60 * 60 * 24);
                    } else {
                        userC.setMaxAge(0);
                        passC.setMaxAge(0);
                    }
                    response.addCookie(passC);
                    response.addCookie(userC);

                    HttpSession session = request.getSession();  
                    session.setAttribute("username", usern);
                    session.setAttribute("user", user);
                    request.getSession().setAttribute("user", user);
                    out.print(user.getUsername() + " " + user.getPassword());
                    request.setAttribute("go", null);
                    response.sendRedirect("home");
//                    request.getRequestDispatcher("home").forward(request, response);
                }
            }
            // Đăng ký
            if (service.equals("signup")) {
                String submit = request.getParameter("submit");
                if (submit == null) {
                    request.getRequestDispatcher("SignUp.jsp").forward(request, response);
                } else {
                    String username = request.getParameter("username");
                    String name = request.getParameter("name");
                    String password = request.getParameter("password");
                    String repassword = request.getParameter("repassword");
                    String email = request.getParameter("email");
                    String phone = request.getParameter("phone");
                    String address = request.getParameter("address");
                    String dob = request.getParameter("dob");
                    int role = Integer.parseInt(request.getParameter("role"));
                    int gender = Integer.parseInt(request.getParameter("gender"));

                    User u = UserDAO.checkUser(username);
                    if (u == null) {
                        SendMail sm = new SendMail();
                        request.setAttribute("mes", "Vui lòng kiểm tra email!");
                        request.setAttribute("username", username);
                        request.setAttribute("name", name);
                        request.setAttribute("password", password);
                        request.setAttribute("email", email);
                        request.setAttribute("phone", phone);
                        request.setAttribute("address", address);
                        request.setAttribute("dob", dob);
                        request.setAttribute("gender", gender);
                        request.setAttribute("role", role);
                        String ma = rdPass();
                        sm.send(email, "Verify Mail", "Your code: " + ma, "he15124399@gmail.com", "pskqnpkwbgirlhoh");
                        request.setAttribute("ma", ma);
                        request.getRequestDispatcher("VerifyMail.jsp").forward(request, response);
                    } else {
                        request.setAttribute("username", username);
                        request.setAttribute("name", name);
                        request.setAttribute("password", password);
                        request.setAttribute("repassword", repassword);
                        request.setAttribute("email", email);
                        request.setAttribute("phone", phone);
                        request.setAttribute("address", address);
                        request.setAttribute("dob", dob);
                        request.setAttribute("q", "Tên tài khoản này đã tồn tại!");
                        request.getRequestDispatcher("SignUp.jsp").forward(request, response);
                    }
                }

            }

            if (service.equals("verifymail")) {
                String submit = request.getParameter("submit");
                if (submit == null) {
                    request.getRequestDispatcher("VerifyMail.jsp").forward(request, response);
                } else {
                    String username = request.getParameter("username");
                    String name = request.getParameter("name");
                    String password = request.getParameter("password");
                    String email = request.getParameter("email");
                    String phone = request.getParameter("phone");
                    String address = request.getParameter("address");
                    String dob = request.getParameter("dob");
                    int role = Integer.parseInt(request.getParameter("role"));
                    int gender = Integer.parseInt(request.getParameter("gender"));
                    String ma = request.getParameter("ma");
                    String code = request.getParameter("code");
                    if (ma.equals(code)) {
                        UserDAO.signUp(username, password, email, name, gender, phone, address, dob, "", role);
                        request.setAttribute("messs", "Đăng ký thành công!");
                        request.getRequestDispatcher("Login.jsp").forward(request, response);
                    } else {
                        request.setAttribute("messs", "Mã xác nhận không đúng!");
                        request.setAttribute("mes", "Vui lòng kiểm tra email!");
                        request.setAttribute("username", username);
                        request.setAttribute("name", name);
                        request.setAttribute("password", password);
                        request.setAttribute("email", email);
                        request.setAttribute("phone", phone);
                        request.setAttribute("address", address);
                        request.setAttribute("dob", dob);
                        request.setAttribute("gender", gender);
                        request.setAttribute("role", role);
                        request.setAttribute("ma", ma);
                        request.getRequestDispatcher("VerifyMail.jsp").forward(request, response);

                    }
                }
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);


    }
    }
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}