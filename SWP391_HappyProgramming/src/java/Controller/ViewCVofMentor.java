/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DAO.UserDAO;
import Model.User;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Admin
 */
public class ViewCVofMentor extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ViewCVofMentor</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ViewCVofMentor at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");

        HttpSession session = request.getSession();
        String username = request.getParameter("username");
        UserDAO dao = new UserDAO();
        User u = dao.getUserByUsername(username);
        User u5star = dao.getallStar(username, 5);
        User u4star = dao.getallStar(username, 4);
        User u3star = dao.getallStar(username, 3);
        User u2star = dao.getallStar(username, 2);

        User u1star = dao.getallStar(username, 1);
        session.setAttribute("mentor", u);
        session.setAttribute("u5star", u5star.getStar());
        session.setAttribute("u4star", u4star.getStar());
        session.setAttribute("u3star", u3star.getStar());
        session.setAttribute("u2star", u2star.getStar());
        session.setAttribute("u1star", u1star.getStar());
        request.getRequestDispatcher("ViewCVofMentor.jsp").forward(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        String username = request.getParameter("mentorname");
        UserDAO dao = new UserDAO();
        User u = dao.getUserByUsername(username);
        User u5star = dao.getallStar(username, 5);
        User u4star = dao.getallStar(username, 4);
        User u3star = dao.getallStar(username, 3);
        User u2star = dao.getallStar(username, 2);
        User u1star = dao.getallStar(username, 1);
        HttpSession session = request.getSession();
        session.setAttribute("mentor", u);
        session.setAttribute("u5star", u5star.getStar());
        session.setAttribute("u4star", u4star.getStar());
        session.setAttribute("u3star", u3star.getStar());
        session.setAttribute("u2star", u2star.getStar());
        session.setAttribute("u1star", u1star.getStar());
        request.getRequestDispatcher("ViewCVofMentor.jsp").forward(request, response);

//        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
